import unittest
import os
from Project_Modules.detector import DetectorGW
import numpy as np

# Change working directory to be able to read files using default arborescence
if os.getcwd() == os.path.dirname(__file__):
    os.chdir("../")


class TestDetectorGW(unittest.TestCase):

    def test_pycbc_psd_wrong_name_instanciation(self):
        with self.assertRaises(ValueError):
            DetectorGW("UKWN")

    def test_detector_in_pycbc(self):
        detector = DetectorGW("LIGO_Design", delta_freq=0.025, low_freq=20.0, high_freq=1023.75)
        self.assertEqual(detector.detector_name, "LIGO_Design")
        self.assertEqual(detector.psd_name, "aLIGODesignSensitivityP1200087")
        self.assertTrue(detector.in_pycbc)
        self.assertTrue(type(detector.psd_data), np.array)

    def test_detector_from_data(self):
        detector = DetectorGW("Livingston_O3a", delta_freq=0.025, low_freq=20.0, high_freq=1023.75)
        self.assertEqual(detector.detector_name, "Livingston_O3a")
        self.assertEqual(detector.psd_name, "Livingston_O3a_psd")
        self.assertFalse(detector.in_pycbc)
        self.assertTrue(type(detector.psd_data), np.array)

    def test_set_delta_Freq(self):
        detector = DetectorGW("LIGO_Design", delta_freq=0.05, low_freq=20.0, high_freq=1023.75)
        self.assertEqual(detector.delta_freq, 0.05)

    def test_delta_freq_too_low(self):
        with self.assertRaises(ValueError):
            DetectorGW("LIGO_Design", delta_freq=0.001, low_freq=20.0, high_freq=1023.75)

    def test_check_delta_freq_inferior_deltafreq_min(self):
        with self.assertRaises(ValueError):
            DetectorGW("LIGO_Design", delta_freq=0.01, low_freq=20.0, high_freq=1023.0)
        self.assertEqual(DetectorGW("LIGO_Design", delta_freq=0.015, low_freq=15.0, high_freq=150.0).delta_freq, 0.015)
        with self.assertRaises(ValueError):
            DetectorGW("Livingston_O3a", delta_freq=0.02, low_freq=15.0, high_freq=150.0)
        self.assertEqual(DetectorGW("Livingston_O3a", delta_freq=0.2, low_freq=20.0, high_freq=150.0).delta_freq, 0.2)

    def test_set_low_freq(self):
        detector = DetectorGW("LIGO_Design", delta_freq=0.025, low_freq=50.0, high_freq=1023.75)
        self.assertEqual(detector.low_freq, 50.0)
        detector = DetectorGW("Livingston_O3a", delta_freq=0.025, low_freq=50.0, high_freq=1023.75)
        self.assertEqual(detector.low_freq, 50.0)

    def test_low_freq_too_low(self):
        with self.assertRaises(ValueError):
            DetectorGW("LIGO_Design", delta_freq=0.025, low_freq=0.01, high_freq=1023.75)
        with self.assertRaises(ValueError):
            DetectorGW("Livingston_O3a", delta_freq=0.025, low_freq=0.01, high_freq=1023.75)

    def test_set_high_freq(self):
        detector = DetectorGW("LIGO_Design", delta_freq=0.025, low_freq=20.0, high_freq=1000.0)
        self.assertEqual(detector.high_freq, 1000.0)
        detector = DetectorGW("Livingston_O3a", delta_freq=0.025, low_freq=20.0, high_freq=1000.0)
        self.assertEqual(detector.high_freq, 1000.0)

    def test_high_freq_too_high(self):
        with self.assertRaises(ValueError):
            DetectorGW("LIGO_Design", delta_freq=0.025, low_freq=20.0, high_freq=10000.0)
        with self.assertRaises(ValueError):
            DetectorGW("Livingston_O3a", delta_freq=0.025, low_freq=20.0, high_freq=10000.0)

    def test_high_freq_inf_low_freq(self):
        with self.assertRaises(ValueError):
            DetectorGW("LIGO_Design", delta_freq=0.025, low_freq=20.0, high_freq=10.0)
        with self.assertRaises(ValueError):
            DetectorGW("Livingston_O3a", delta_freq=0.025, low_freq=20.0, high_freq=10.0)

    def test_delta_freq_incompatible_with_low_high_freq(self):
        with self.assertRaises(ValueError):
            DetectorGW("LIGO_Design", delta_freq=0.3, low_freq=20.0, high_freq=1023.75)
        with self.assertRaises(ValueError):
            DetectorGW("LIGO_Design", delta_freq=0.3, low_freq=30.0, high_freq=1023.75)
        with self.assertRaises(ValueError):
            DetectorGW("LIGO_Design", delta_freq=0.3, low_freq=20.0, high_freq=300.0)
        detector = DetectorGW("LIGO_Design", delta_freq=0.3, low_freq=30.0, high_freq=300.0)
        self.assertEqual(detector.delta_freq, 0.3)

        with self.assertRaises(ValueError):
            DetectorGW("Livingston_O3a", delta_freq=0.3, low_freq=20.0, high_freq=1023.75)
        with self.assertRaises(ValueError):
            DetectorGW("Livingston_O3a", delta_freq=0.3, low_freq=30.0, high_freq=1023.75)
        with self.assertRaises(ValueError):
            DetectorGW("Livingston_O3a", delta_freq=0.3, low_freq=20.0, high_freq=300.0)
        detector = DetectorGW("Livingston_O3a", delta_freq=0.3, low_freq=30.0, high_freq=300.0)
        self.assertEqual(detector.delta_freq, 0.3)

    def test_length(self):
        detector = DetectorGW("LIGO_Design", delta_freq=5.0, low_freq=20.0, high_freq=1000.0)
        self.assertEqual(detector.length, 201)
        detector = DetectorGW("LIGO_Design", delta_freq=5.0, low_freq=100.0, high_freq=1000.0)
        self.assertEqual(detector.length, 201)
        detector = DetectorGW("LIGO_Design", delta_freq=1.0, low_freq=20.0, high_freq=1000.0)
        self.assertEqual(detector.length, 1001)
        detector = DetectorGW("LIGO_Design", delta_freq=0.25, low_freq=20.0, high_freq=800.0)
        self.assertEqual(detector.length, 3201)
        detector = DetectorGW("Livingston_O3a", delta_freq=5.0, low_freq=20.0, high_freq=1000.0)
        self.assertEqual(detector.length, 201)
        detector = DetectorGW("Livingston_O3a", delta_freq=5.0, low_freq=100.0, high_freq=1000.0)
        self.assertEqual(detector.length, 201)
        detector = DetectorGW("Livingston_O3a", delta_freq=1.0, low_freq=20.0, high_freq=1000.0)
        self.assertEqual(detector.length, 1001)
        detector = DetectorGW("Livingston_O3a", delta_freq=0.25, low_freq=20.0, high_freq=800.0)
        self.assertEqual(detector.length, 3201)

    def test_cutoff(self):
        detector = DetectorGW("LIGO_Design", delta_freq=5.0, low_freq=20.0, high_freq=1000.0)
        np.testing.assert_array_equal(detector.psd_data.numpy()[0:4], np.zeros(4))
        self.assertNotEqual(detector.psd_data[4], 0.0)
        detector = DetectorGW("Livingston_O3a", delta_freq=1.0, low_freq=30.0, high_freq=1000.0)
        np.testing.assert_array_equal(detector.psd_data.numpy()[0:30], np.zeros(30))
        self.assertNotEqual(detector.psd_data[30], 0.0)
