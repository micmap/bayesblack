import unittest
import numpy as np
from Project_Modules.utility_functions import *


class TestUtilityFunctions(unittest.TestCase):
    def test_mapping_masses_numbers(self):
        m1 = 12.0
        m2 = 5.0
        mc, q = m1_m2_to_mc_q(m1, m2)
        m1_mapped, m2_mapped = mc_q_to_m1_m2(mc, q)

        self.assertAlmostEqual(m1, m1_mapped)
        self.assertAlmostEqual(m2, m2_mapped)

        m1 = 2.0
        m2 = 25.0
        mc, q = m1_m2_to_mc_q(m1, m2)
        m1_mapped, m2_mapped = mc_q_to_m1_m2(mc, q)

        self.assertAlmostEqual(m1, m1_mapped)
        self.assertAlmostEqual(m2, m2_mapped)

        m1 = 178.348
        m2 = 65.7639
        mc, q = m1_m2_to_mc_q(m1, m2)
        m1_mapped, m2_mapped = mc_q_to_m1_m2(mc, q)

        self.assertAlmostEqual(m1, m1_mapped)
        self.assertAlmostEqual(m2, m2_mapped)

    def test_mapping_masses_arrays(self):
        m1 = np.array([4.874, 8.3423, 104.4876])
        m2 = np.array([7.2345, 12.222, 0.874])
        mc, q = m1_m2_to_mc_q(m1, m2)
        m1_mapped, m2_mapped = mc_q_to_m1_m2(mc, q)
        np.testing.assert_almost_equal(m1.tolist(), m1_mapped.tolist())
        np.testing.assert_almost_equal(m2.tolist(), m2_mapped.tolist())

    def test_parallel_array_range(self):
        ranges = parallel_array_range(length=8, n_cpu=2)
        self.assertEqual(ranges, [(0, 4), (4, 8)])
        ranges = parallel_array_range(length=8, n_cpu=3)
        self.assertEqual(ranges, [(0, 3), (3, 6), (6, 8)])
        ranges = parallel_array_range(length=3, n_cpu=3)
        self.assertEqual(ranges, [(0, 1), (1, 2), (2, 3)])
        with self.assertRaises(IndexError):
            parallel_array_range(length=1, n_cpu=2)
        ranges = parallel_array_range(length=8, n_cpu=1)
        self.assertEqual(ranges, [(0, 8)])

    def test_clean_path(self):
        self.assertEqual(clean_path("test_path"), "test_path/")
        self.assertEqual(clean_path("test_path/"), "test_path/")

    def test_cos_theta_isolated_tilt(self):

        np.testing.assert_array_equal(cos_theta_isolated_tilt(cos_nu1=np.array([0.0, 0.5]),
                                                              cos_nu2=np.array([1.0, 1.0])), np.array([0.0, 0.5]))
        with self.assertRaises(IndexError):
            cos_theta_isolated_tilt(cos_nu1=np.array([0.0, 0.5]), cos_nu2=np.array([0.0, 0.5, 0.3]))

    def test_comp_chieff(self):

        np.testing.assert_array_equal(comp_chieff(m1=np.array([1.0, 3.0, 5.0]), m2=np.array([1.0, 1.0, 2.0]),
                                                  chi1=np.array([0.5, 1.0, 1.0]), chi2=np.array([0.5, 0.5, 1.0]),
                                                  cos_theta_1=np.array([1.0, 0.5, 0.0]),
                                                  cos_theta_2=np.array([1.0, 0.5, 0.5])),
                                      np.array([0.5, 7.0/16.0, 1.0/7.0]))
