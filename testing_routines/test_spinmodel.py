import os
import unittest
from scipy.stats import maxwell

from Project_Modules.spin_model import SpinModel
from Project_Modules.utility_functions import SchemaErrorVerbose

# Change working directory to be able to read files using default arborescence
if os.getcwd() == os.path.dirname(__file__):
    os.chdir("../")


class TestSpinModel(unittest.TestCase):

    def test_wrong_name_model(self):
        with self.assertRaises(ValueError):
            SpinModel("UNKNOWN")

    def test_wrong_magnitude_generator_input_structure(self):
        mag_gen_param = {"name_dist": "Maxwellian", "parameters": [0.2]}
        with self.assertRaises(SchemaErrorVerbose):
            SpinModel("Aligned", mag_gen_param=mag_gen_param)
        mag_gen_param = {"name_dist": "Maxwellian", "parameters": 0.2}
        with self.assertRaises(SchemaErrorVerbose):
            SpinModel("Aligned", mag_gen_param=mag_gen_param)
        with self.assertRaises(SchemaErrorVerbose):
            SpinModel("Aligned", mag_gen_param="Maxwellian")

    def test_wrong_name_magnitude_generator(self):
        with self.assertRaises(ValueError):
            SpinModel("Aligned", mag_gen_param={"name_dist": "UNKNOWN", "parameters": {"sigma": 0.2}})

    def test_magnitude_maxwellian(self):
        spin = SpinModel("Aligned", mag_gen_param={"name_dist": "Maxwellian", "parameters": {"sigma": 0.2}})
        self.assertEqual(type(spin.mag_gen), type(maxwell()))

    def test_aligned_spin(self):
        spin = SpinModel("Aligned")
        self.assertEqual(spin.name_model, "Aligned")

    def test_isotropic_spin(self):
        spin = SpinModel("Isotropic")
        self.assertEqual(spin.name_model, "Isotropic")

    def test_isolated_tilt_spin(self):
        spin = SpinModel("Isolated_tilt")
        self.assertEqual(spin.name_model, "Isolated_tilt")

    def test_isotropic_hierarchical_spin(self):
        spin = SpinModel("Isotropic_Hierarchical")
        self.assertEqual(spin.name_model, "Isotropic_Hierarchical")



