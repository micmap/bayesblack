import unittest
import os
import numpy as np
import shutil

import Project_Modules.auxiliary_cosmorate as acr

dir_cr = "cosmo_rate_files/"
num_files = 10
num_var_cat = 14
identifier_cat = "field"
header_catalog = "ID\tmrem1\tmrem2\tcmu1\tcmu2\tchi1\tchi2\tth1\tth2\tM_SC\tz_merg\tz_form" \
                 "\ttime_delay [yr]\tZ progenitor"
good_header_catalog = "id\tm1\tm2\tcos_nu1\tcos_nu2\tchi1\tchi2\ttheta1\ttheta2\tm_sc\tz\tz_form" \
                      "\ttime_delay\tz_progenitor"
header_mr = "Redshift\tMerger Rate Density [Gpc^-3yr^-1]"
header_zperc = "Redshift\t[0.0002\t0.0004\t0.0008\t0.0012\t0.0016\t0.002\t0.004\t0.006\t0.008\t0.012\t0.016\t0.02]" \
                 "\tTotal percentage"

filename_mr = "MRD_spread_12Z_40_No_MandF2017_0.4_No_No_0.dat"
filename_zperc = "Zperc_z_12Z_40_No_MandF2017_0.4_No_No_50_0.dat"


class TestObservingRun(unittest.TestCase):

    def setUp(self):

        # Check that the working directory is the good one
        if os.getcwd() != os.path.dirname(__file__):
            os.chdir(os.path.dirname(__file__))

        # Create a directo
        os.mkdir(dir_cr)

        # Create catalog files
        data = np.random.rand(5, num_var_cat)
        list_name_cr_files = ["cat_" + identifier_cat + "_" + str((i+1)) + "_50.dat" for i in range(num_files)]
        for filename in list_name_cr_files:
            np.savetxt(dir_cr + filename, data, header=header_catalog)

        # Create Merger rate density file
        data = np.random.rand(5, 2)
        np.savetxt(dir_cr + filename_mr, data, header=header_mr)

        # Create Z percentage file
        data = np.random.rand(5, 14)
        np.savetxt(dir_cr + filename_zperc, data, header=header_zperc)

    def tearDown(self):

        # Check that the working directory is the good one
        if os.getcwd() != os.path.dirname(__file__):
            os.chdir(os.path.dirname(__file__))

        # Remove temporary directory
        shutil.rmtree(dir_cr)

    def test_logCR(self):

        logfile_created = acr.LogFileCR(dir_cr)
        self.assertEqual(logfile_created.path_file, dir_cr + "log_file_cr.in")
        self.assertEqual(logfile_created.status, {"files_moved": False, "header_rewritten": False})

        logfile_created.status["files_moved"] = True
        logfile_created.update()
        logfile_read = acr.LogFileCR(dir_cr)
        self.assertEqual(logfile_read.status, logfile_created.status)

    def test_prepare_directory_nothing_happen(self):
        logfile = acr.LogFileCR(dir_cr)
        logfile.status["files_moved"] = True
        acr.prepare_directory(dir_cr, logfile)
        self.assertFalse(os.path.exists(dir_cr + "catalogs"))

    def test_prepare_directory_normal_usage(self):

        logfile = acr.LogFileCR(dir_cr)
        acr.prepare_directory(dir_cr, logfile)
        self.assertTrue(os.path.exists(dir_cr + "catalogs"))
        self.assertTrue(os.listdir(dir_cr + "catalogs"))
        self.assertTrue(os.path.isfile(dir_cr+filename_mr))
        self.assertTrue(os.path.isfile(dir_cr+filename_zperc))
        self.assertEqual(logfile.status, {"files_moved": True, "header_rewritten": False})

    def test_rewrite_header_cosmorate_nothing_changed(self):
        logfile = acr.LogFileCR(dir_cr)
        logfile.status = {"files_moved": True, "header_rewritten": True}
        acr.rewrite_header_cosmorate(dir_cr, logfile, num_var_cat)

        with open(dir_cr + "cat_" + identifier_cat + "_1_50.dat", "r") as filein:
            list_of_lines = filein.readlines()
            self.assertEqual(list_of_lines[0], "# " + header_catalog + "\n")

    def test_rewrite_header_cosmorate_files_not_moved(self):
        logfile = acr.LogFileCR(dir_cr)
        with self.assertRaises(FileNotFoundError):
            acr.rewrite_header_cosmorate(dir_cr, logfile, num_var_cat)

    def test_rewrite_header_cosmorate_normal_usage(self):
        logfile = acr.LogFileCR(dir_cr)
        acr.prepare_directory(dir_cr, logfile)
        acr.rewrite_header_cosmorate(dir_cr, logfile, num_var_cat)

        list_name_cr_files = ["cat_" + identifier_cat + "_" + str((i + 1)) + "_50.dat" for i in range(num_files)]
        for filename in list_name_cr_files:
            with open(dir_cr + "catalogs/" + filename, "r") as filein:
                list_of_lines = filein.readlines()
                self.assertEqual(list_of_lines[0], good_header_catalog + "\n")

        self.assertTrue(os.path.isfile(dir_cr + filename_mr))
        self.assertTrue(os.path.isfile(dir_cr + filename_zperc))
        self.assertEqual(logfile.status, {"files_moved": True, "header_rewritten": True})

    def test_rewrite_header_cosmorate_wrong_delimiter_cr(self):
        logfile = acr.LogFileCR(dir_cr)
        acr.prepare_directory(dir_cr, logfile)
        with self.assertRaises(IndexError):
            acr.rewrite_header_cosmorate(dir_cr, logfile, num_var_cat, delimiter_cr=",")

    def test_rewrite_header_cosmorate_wrong_variable_number(self):
        logfile = acr.LogFileCR(dir_cr)
        acr.prepare_directory(dir_cr, logfile)
        with self.assertRaises(IndexError):
            acr.rewrite_header_cosmorate(dir_cr, logfile, num_var_cat+1, delimiter_cr=",")

    def test_rewrite_header_cosmorate_change_delimiter_catalog(self):
        logfile = acr.LogFileCR(dir_cr)
        acr.prepare_directory(dir_cr, logfile)
        acr.rewrite_header_cosmorate(dir_cr, logfile, num_var_cat, delimiter_new_cat=",")

        modified_header = ",".join(good_header_catalog.split("\t"))
        list_name_cr_files = ["cat_" + identifier_cat + "_" + str((i + 1)) + "_50.dat" for i in range(num_files)]
        for filename in list_name_cr_files:
            with open(dir_cr + "catalogs/" + filename, "r") as filein:
                list_of_lines = filein.readlines()
                self.assertEqual(list_of_lines[0], modified_header + "\n")

        self.assertTrue(os.path.isfile(dir_cr + filename_mr))
        self.assertTrue(os.path.isfile(dir_cr + filename_zperc))
        self.assertEqual(logfile.status, {"files_moved": True, "header_rewritten": True})

    def test_rewrite_header_cosmorate_already_remapped_without_noted_in_logfile(self):
        logfile = acr.LogFileCR(dir_cr)
        acr.prepare_directory(dir_cr, logfile)
        acr.rewrite_header_cosmorate(dir_cr, logfile, num_var_cat)
        logfile.status["header_rewritten"] = False

        acr.rewrite_header_cosmorate(dir_cr, logfile, num_var_cat)

        list_name_cr_files = ["cat_" + identifier_cat + "_" + str((i + 1)) + "_50.dat" for i in range(num_files)]
        for filename in list_name_cr_files:
            with open(dir_cr + "catalogs/" + filename, "r") as filein:
                list_of_lines = filein.readlines()
                self.assertEqual(list_of_lines[0], good_header_catalog + "\n")

        self.assertTrue(os.path.isfile(dir_cr + filename_mr))
        self.assertTrue(os.path.isfile(dir_cr + filename_zperc))
        self.assertEqual(logfile.status, {"files_moved": True, "header_rewritten": True})

    def test_rewrite_header_cosmorate_variable_mapping_error_raised(self):
        logfile = acr.LogFileCR(dir_cr)
        acr.prepare_directory(dir_cr, logfile)
        list_name_cr_files = ["cat_" + identifier_cat + "_" + str((i + 1)) + "_50.dat" for i in range(num_files)]
        for filename in list_name_cr_files:
            with open(dir_cr + "catalogs/" + filename, "r") as filein:
                list_of_lines = filein.readlines()
                list_of_lines[0] = "ID\tUNKNOWN\n"
            with open(dir_cr + "catalogs/" + filename, "w") as filein:
                filein.writelines(list_of_lines)

        with self.assertRaises(acr.MappingError):
            acr.rewrite_header_cosmorate(dir_cr, logfile, 2)

    def test_process_cosmorate_wrong_directory_cr(self):

        with self.assertRaises(FileNotFoundError):
            acr.process_cosmorate(dir_cr+"?", num_var_cat)

    def test_process_cosmorate_normal_usage(self):

        acr.process_cosmorate(dir_cr, num_var_cat)
        self.assertTrue(os.path.isfile(dir_cr + "log_file_cr.in"))
        logfile_read = acr.LogFileCR(dir_cr)
        self.assertTrue(os.path.exists(dir_cr + "catalogs"))
        self.assertTrue(os.listdir(dir_cr + "catalogs"))
        self.assertTrue(os.path.isfile(dir_cr + filename_mr))
        self.assertTrue(os.path.isfile(dir_cr + filename_zperc))
        list_name_cr_files = ["cat_" + identifier_cat + "_" + str((i + 1)) + "_50.dat" for i in range(num_files)]
        for filename in list_name_cr_files:
            with open(dir_cr + "catalogs/" + filename, "r") as filein:
                list_of_lines = filein.readlines()
                self.assertEqual(list_of_lines[0], good_header_catalog + "\n")
        self.assertEqual(logfile_read.status, {"files_moved": True, "header_rewritten": True})
