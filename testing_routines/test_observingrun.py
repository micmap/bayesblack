import datetime
import os
import unittest

from Project_Modules.gw_event import GwEvent
from Project_Modules.observing_run import ObservingRun

# Change working directory to be able to read files using default arborescence
if os.getcwd() == os.path.dirname(__file__):
    os.chdir("../")


class TestObservingRun(unittest.TestCase):

    def test_initialisation_o1(self):

        # Test that initialisation is properly working for O1, O2 and O3a
        obs_o1 = ObservingRun("O1", read_data_posterior=False, read_data_prior=False)
        true_list_bbh = ['GW150914', 'GW151012', 'GW151226']
        true_list_bns = []
        true_list_bhns = []

        # Check that number of detections is good
        self.assertEqual(obs_o1.n_det["BBH"], 3)
        self.assertEqual(obs_o1.n_det["BNS"], 0)
        self.assertEqual(obs_o1.n_det["BHNS"], 0)

        # Check that list of gw_events_name is good
        self.assertEqual(obs_o1.gw_events_name["BBH"], true_list_bbh)
        self.assertEqual(obs_o1.gw_events_name["BNS"], true_list_bns)
        self.assertEqual(obs_o1.gw_events_name["BHNS"], true_list_bhns)

        # Check that the keys of gw_events are the events_name
        self.assertEqual(obs_o1.gw_events["BBH"].keys(), dict.fromkeys(true_list_bbh).keys())
        self.assertEqual(obs_o1.gw_events["BNS"].keys(), dict.fromkeys(true_list_bns).keys())
        self.assertEqual(obs_o1.gw_events["BHNS"].keys(), dict.fromkeys(true_list_bhns).keys())

        # Check that the events in gw_events are of type GwEvent (only BBH for O1)
        for key in obs_o1.gw_events["BBH"]:
            self.assertTrue(type(obs_o1.gw_events["BBH"][key] == GwEvent))

    def test_initialisation_o2(self):

        # Test that initialisation is properly working for O1, O2 and O3a
        obs_o2 = ObservingRun("O2", read_data_posterior=False, read_data_prior=False)
        true_list_bbh = ['GW170104', 'GW170608', 'GW170729', 'GW170809', 'GW170814', 'GW170818', 'GW170823']
        true_list_bns = ['GW170817']
        true_list_bhns = []

        # Check that number of detections is good
        self.assertEqual(obs_o2.n_det["BBH"], 7)
        self.assertEqual(obs_o2.n_det["BNS"], 1)
        self.assertEqual(obs_o2.n_det["BHNS"], 0)

        # Check that list of gw_events_name is good
        self.assertEqual(obs_o2.gw_events_name["BBH"], true_list_bbh)
        self.assertEqual(obs_o2.gw_events_name["BNS"], true_list_bns)
        self.assertEqual(obs_o2.gw_events_name["BHNS"], true_list_bhns)

        # Check that the keys of gw_events are the events_name
        self.assertEqual(obs_o2.gw_events["BBH"].keys(), dict.fromkeys(true_list_bbh).keys())
        self.assertEqual(obs_o2.gw_events["BNS"].keys(), dict.fromkeys(true_list_bns).keys())
        self.assertEqual(obs_o2.gw_events["BHNS"].keys(), dict.fromkeys(true_list_bhns).keys())

        # Check that the events in gw_events are of type GwEvent (only BBH and BNS for O2)
        for key in obs_o2.gw_events["BBH"]:
            self.assertTrue(type(obs_o2.gw_events["BBH"][key] == GwEvent))
        for key in obs_o2.gw_events["BNS"]:
            self.assertTrue(type(obs_o2.gw_events["BNS"][key] == GwEvent))

    def test_initialisation_o3a(self):

        # Test that initialisation is properly working for O1, O2 and O3a
        obs_o3a = ObservingRun("O3a", read_data_posterior=False, read_data_prior=False)
        true_list_bbh = ['GW190408_181802', 'GW190412', 'GW190413_052954', 'GW190413_134308', 'GW190421_213856',
                         'GW190424_180648', 'GW190503_185404', 'GW190512_180714', 'GW190513_205428', 'GW190514_065416',
                         'GW190517_055101', 'GW190519_153544', 'GW190521', 'GW190521_074359', 'GW190527_092055',
                         'GW190602_175927', 'GW190620_030421', 'GW190630_185205', 'GW190701_203306', 'GW190706_222641',
                         'GW190707_093326', 'GW190708_232457', 'GW190720_000836', 'GW190727_060333', 'GW190728_064510',
                         'GW190731_140936', 'GW190803_022701', 'GW190814', 'GW190828_063405', 'GW190828_065509',
                         'GW190910_112807', 'GW190915_235702', 'GW190924_021846', 'GW190929_012149', 'GW190930_133541']
        true_list_bns = ["GW190425"]
        true_list_bhns = []

        # Check that number of detections is good
        self.assertEqual(obs_o3a.n_det["BBH"], 35)
        self.assertEqual(obs_o3a.n_det["BNS"], 1)
        self.assertEqual(obs_o3a.n_det["BHNS"], 0)

        # Check that list of gw_events_name is good
        self.assertEqual(obs_o3a.gw_events_name["BBH"], true_list_bbh)
        self.assertEqual(obs_o3a.gw_events_name["BNS"], true_list_bns)
        self.assertEqual(obs_o3a.gw_events_name["BHNS"], true_list_bhns)

        # Check that the keys of gw_events are the events_name
        self.assertEqual(obs_o3a.gw_events["BBH"].keys(), dict.fromkeys(true_list_bbh).keys())
        self.assertEqual(obs_o3a.gw_events["BNS"].keys(), dict.fromkeys(true_list_bns).keys())
        self.assertEqual(obs_o3a.gw_events["BHNS"].keys(), dict.fromkeys(true_list_bhns).keys())

        # Check that the events in gw_events are of type GwEvent (only BBH and BNS for O2)
        for key in obs_o3a.gw_events["BBH"]:
            self.assertTrue(type(obs_o3a.gw_events["BBH"][key] == GwEvent))
        for key in obs_o3a.gw_events["BNS"]:
            self.assertTrue(type(obs_o3a.gw_events["BNS"][key] == GwEvent))

    def test_initialisation_wrong(self):
        with self.assertRaises(ValueError):
            ObservingRun("O27")
        with self.assertRaises(ValueError):
            ObservingRun("O3")
        with self.assertRaises(ValueError):
            ObservingRun("o1")

    def test_co_only_obs(self):

        # Keep only the BBH of O2
        o2 = ObservingRun("O2", co_only="BBH", read_data_posterior=False, read_data_prior=False)
        self.assertEqual(o2.co_only, "BBH")
        self.assertEqual(o2.n_det["BBH"], 7)
        self.assertEqual(o2.n_det["BNS"], 0)
        self.assertEqual(o2.n_det["BHNS"], 0)
        self.assertEqual(o2.gw_events_name["BBH"], ['GW170104', 'GW170608', 'GW170729', 'GW170809', 'GW170814',
                                                    'GW170818', 'GW170823'])
        self.assertEqual(o2.gw_events_name["BNS"], [])
        self.assertEqual(o2.gw_events_name["BHNS"], [])
        self.assertEqual(o2.modif_status["num_event_removed"]["BBH"], 0)
        self.assertEqual(o2.modif_status["num_event_removed"]["BNS"], 1)
        self.assertEqual(o2.modif_status["num_event_removed"]["BHNS"], 0)

        # Keep only the BNS of O3a
        o3a = ObservingRun("O3a", co_only="BNS", read_data_posterior=False, read_data_prior=False)
        self.assertEqual(o3a.co_only, "BNS")
        self.assertEqual(o3a.n_det["BBH"], 0)
        self.assertEqual(o3a.n_det["BNS"], 1)
        self.assertEqual(o3a.n_det["BHNS"], 0)
        self.assertEqual(o3a.gw_events_name["BBH"], [])
        self.assertEqual(o3a.gw_events_name["BNS"], ['GW190425'])
        self.assertEqual(o3a.gw_events_name["BHNS"], [])
        self.assertEqual(o3a.modif_status["num_event_removed"]["BBH"], 35)
        self.assertEqual(o3a.modif_status["num_event_removed"]["BNS"], 0)
        self.assertEqual(o3a.modif_status["num_event_removed"]["BHNS"], 0)

        # Check wrong initialisation
        with self.assertRaises(ValueError):
            ObservingRun("O2", co_only="a", read_data_posterior=False, read_data_prior=False)
        with self.assertRaises(ValueError):
            ObservingRun("O2", co_only="BBHs", read_data_posterior=False, read_data_prior=False)
        with self.assertRaises(ValueError):
            ObservingRun("O2", co_only=["BBHs"], read_data_posterior=False, read_data_prior=False)


    def test_remove_event(self):
        o1 = ObservingRun("O1", events_to_remove={"BBH": ["GW151012"]}, read_data_posterior=False,
                          read_data_prior=False)

        # Check attributes change when removing one event
        self.assertEqual(o1.n_det["BBH"], 2)
        self.assertEqual(o1.gw_events_name["BBH"], ['GW150914', 'GW151226'])
        self.assertEqual(o1.gw_events["BBH"].keys(), {'GW150914': [], 'GW151226': []}.keys())
        self.assertTrue(o1.modif_status["flag"])
        self.assertListEqual(o1.modif_status["list_event_removed"]["BBH"], ["GW151012"])
        self.assertEqual(o1.modif_status["num_event_removed"]["BBH"], 1)
        self.assertEqual(o1.modif_status["num_event_removed"]["BNS"], 0)
        self.assertEqual(o1.modif_status["num_event_removed"]["BHNS"], 0)

        # Edge-case: event name not in list
        with self.assertRaises(ValueError):
            ObservingRun("O1", events_to_remove={"BBH": ["GW180024"]})

        # Edge-cases: events_to_remove must be a dictionary of list
        with self.assertRaises(TypeError):  # no dictionary
            ObservingRun("O1", events_to_remove=["GW151012"])
        with self.assertRaises(TypeError):  # no list
            ObservingRun("O1", events_to_remove={"BBH": "GW151012"})
        with self.assertRaises(ValueError):  # wrong key
            ObservingRun("O1", events_to_remove={"BBHs": "GW151012"})

    def test_keep_only_events(self):

        o2 = ObservingRun("O2", read_data_posterior=False, read_data_prior=False)
        o2.keep_only_events({"BBH": ['GW170608', 'GW170729', 'GW170818']})
        self.assertEqual(o2.n_det["BBH"], 3)
        self.assertEqual(o2.n_det["BNS"], 0)
        self.assertEqual(o2.gw_events_name["BBH"], ['GW170608', 'GW170729', 'GW170818'])
        self.assertEqual(o2.gw_events_name["BNS"], [])
        self.assertEqual(o2.gw_events_name["BHNS"], [])
        self.assertEqual(o2.modif_status["num_event_removed"]["BBH"], 4)
        self.assertEqual(o2.modif_status["num_event_removed"]["BNS"], 1)
        self.assertEqual(o2.modif_status["num_event_removed"]["BHNS"], 0)
        self.assertEqual(o2.gw_events["BBH"].keys(), dict.fromkeys(['GW170608', 'GW170729', 'GW170818']).keys())
        self.assertEqual(o2.gw_events["BNS"].keys(), dict.fromkeys([]).keys())
        self.assertEqual(o2.gw_events["BHNS"].keys(), dict.fromkeys([]).keys())

    def test_keep_events_from_date(self):

        # Keep only events in the last month of O2 (August)
        o2 = ObservingRun("O2", read_data_posterior=False, read_data_prior=False,
                          dates_keep_only=(datetime.date(2017, 8, 1), datetime.date(2017, 8, 25)))
        self.assertEqual(o2.n_det["BBH"], 4)
        self.assertEqual(o2.n_det["BNS"], 1)
        self.assertEqual(o2.n_det["BHNS"], 0)
        self.assertEqual(o2.gw_events_name["BBH"], ['GW170809', 'GW170814', 'GW170818', 'GW170823'])
        self.assertEqual(o2.gw_events_name["BNS"], ['GW170817'])
        self.assertEqual(o2.gw_events_name["BHNS"], [])
        self.assertEqual(o2.modif_status["num_event_removed"]["BBH"], 3)
        self.assertEqual(o2.modif_status["num_event_removed"]["BNS"], 0)
        self.assertEqual(o2.modif_status["num_event_removed"]["BHNS"], 0)
        self.assertEqual(o2.gw_events["BBH"].keys(),
                         dict.fromkeys(['GW170809', 'GW170814', 'GW170818', 'GW170823']).keys())
        self.assertEqual(o2.gw_events["BNS"].keys(), dict.fromkeys(['GW170817']).keys())
        self.assertEqual(o2.gw_events["BHNS"].keys(), dict.fromkeys([]).keys())
        self.assertTrue(o2.modif_status["flag"])

        # Edge-case : start and end date contain one event
        o2 = ObservingRun("O2", read_data_posterior=False, read_data_prior=False,
                          dates_keep_only=(datetime.date(2017, 8, 14), datetime.date(2017, 8, 23)))
        self.assertEqual(o2.n_det["BBH"], 3)
        self.assertEqual(o2.n_det["BNS"], 1)
        self.assertEqual(o2.n_det["BHNS"], 0)
        self.assertEqual(o2.gw_events_name["BBH"], ['GW170814', 'GW170818', 'GW170823'])
        self.assertEqual(o2.gw_events_name["BNS"], ['GW170817'])
        self.assertEqual(o2.gw_events_name["BHNS"], [])
        self.assertEqual(o2.modif_status["num_event_removed"]["BBH"], 4)
        self.assertEqual(o2.modif_status["num_event_removed"]["BNS"], 0)
        self.assertEqual(o2.modif_status["num_event_removed"]["BHNS"], 0)
        self.assertEqual(o2.gw_events["BBH"].keys(),
                         dict.fromkeys(['GW170814', 'GW170818', 'GW170823']).keys())
        self.assertEqual(o2.gw_events["BNS"].keys(), dict.fromkeys(['GW170817']).keys())
        self.assertEqual(o2.gw_events["BHNS"].keys(), dict.fromkeys([]).keys())
        self.assertTrue(o2.modif_status["flag"])

        # End date before start date
        with self.assertRaises(ValueError):
            ObservingRun("O2", read_data_posterior=False, read_data_prior=False,
                         dates_keep_only=(datetime.date(2017, 8, 23), datetime.date(2017, 8, 14)))

        # Start date before the start of observing run
        with self.assertRaises(ValueError):
            ObservingRun("O2", read_data_posterior=False, read_data_prior=False,
                        dates_keep_only=(datetime.date(2016, 8, 14), datetime.date(2017, 8, 14)))

        # End date after the end of observing run
        with self.assertRaises(ValueError):
            ObservingRun("O2", read_data_posterior=False, read_data_prior=False,
                        dates_keep_only=(datetime.date(2017, 8, 14), datetime.date(2017, 8, 30)))

        # Wrong type for dates
        with self.assertRaises(TypeError):
            ObservingRun("O2", read_data_posterior=False, read_data_prior=False,
                        dates_keep_only=("2017-8-14", "2017-8-30"))


