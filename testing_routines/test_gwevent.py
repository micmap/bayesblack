import unittest
from Project_Modules.gw_event import GwEvent
import os
import pandas as pd
import datetime

# Change working directory to be able to read files using default arborescence
if os.getcwd() == os.path.dirname(__file__):
    os.chdir("../")


class TestGwEvent(unittest.TestCase):

    def test_initialisation_without_data(self):

        gw_1 = GwEvent("GW150914", read_posterior=False, read_prior=False)

        # Check name, date and variables assigned to default
        self.assertEqual(gw_1.name_event, "GW150914")
        self.assertEqual(gw_1.event_par, ["m1", "m2", "Mc", "Mt", "q", "chieff", "z"])

        # Check that data are not loaded and flags set to False
        self.assertEqual(gw_1.flags_loaded["post"], False)
        self.assertEqual(gw_1.flags_loaded["prior"], False)
        self.assertIsNone(gw_1.data_post)
        self.assertIsNone(gw_1.data_prior)

    def test_initialisation_with_data(self):
        gw_1 = GwEvent("GW190521", read_posterior=True, read_prior=True)

        # Check name, and variables assigned to default
        self.assertEqual(gw_1.name_event, "GW190521")
        self.assertEqual(gw_1.event_par, ["m1", "m2", "Mc","Mt", "q", "chieff", "z"])

        # Check that data are loaded, flags set to True and data type is Pandas dataframe
        self.assertEqual(gw_1.flags_loaded["post"], True)
        self.assertEqual(gw_1.flags_loaded["prior"], True)
        self.assertTrue(type(gw_1.data_post) == pd.DataFrame)
        self.assertTrue(type(gw_1.data_prior) == pd.DataFrame)

        self.assertEqual(list(gw_1.data_post.columns), ["m1", "m2", "Mc","Mt", "q", "chieff", "z"])
        self.assertEqual(list(gw_1.data_prior.columns), ["m1", "m2", "Mc","Mt", "q", "chieff", "z"])

    def test_wrong_initialisation(self):

        # Wrong name or file not present
        with self.assertRaises(FileNotFoundError):
            GwEvent("GW190522")

        # Parameter not accessible
        with self.assertRaises(ValueError):
            GwEvent("GW190521", event_par=["m3"])

    def test_read_data(self):

        # Read with path set to default during instanciation
        gw_1 = GwEvent("GW150914", read_posterior=True, read_prior=True,
                       path_posterior="auxiliary_files/LVC_data/Posterior/",
                       path_prior="auxiliary_files/LVC_data/Prior/")
        self.assertEqual(gw_1.flags_loaded["post"], True)
        self.assertEqual(gw_1.flags_loaded["prior"], True)

        # Read with wrong values of path during instanciation
        with self.assertRaises(FileNotFoundError):
            GwEvent("GW150914", read_posterior=True, read_prior=True,
                       path_posterior="wrong_path/", path_prior="wrong_path")

        # Read with default values for path after instanciation
        gw_1 = GwEvent("GW150914", read_posterior=False, read_prior=False)
        gw_1.read_posterior_data()
        gw_1.read_prior_data()
        self.assertEqual(gw_1.flags_loaded["post"], True)
        self.assertEqual(gw_1.flags_loaded["prior"], True)

        # Read with wrong path values after instanciation
        gw_1 = GwEvent("GW150914", read_posterior=False, read_prior=False)
        with self.assertRaises(FileNotFoundError):
            gw_1.read_posterior_data(path_dir="wrong_path")
        with self.assertRaises(FileNotFoundError):
            gw_1.read_prior_data(path_dir="wrong_path")

    def test_date_event(self):

        self.assertEqual(GwEvent("GW150914", read_posterior=False, read_prior=False).date_event,
                         datetime.date(2015, 9, 14))
        self.assertEqual(GwEvent("GW190521", read_posterior=False, read_prior=False).date_event,
                         datetime.date(2019, 5, 21))
        self.assertEqual(GwEvent("GW190602_175927", read_posterior=False, read_prior=False).date_event,
                         datetime.date(2019, 6, 2))
