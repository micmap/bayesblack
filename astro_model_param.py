# Astrophysical model parameters (hyperparameters)
def return_astro_param(sysargv):

    # Assign the values for the hyper-parameters
    if len(sysargv) <= 1:  # Manual astro-parameter assignment
        co_type = "BBH"
#        form_channel = "Iso"
#        sigma_z = 0.4
#        sigma_spin = 0.1
        alpha = 3.0
#        sn_model = "delayed"
    else:  # Input astro-parameter assignment (useful for bash script)
        co_type = str(sysargv[1])
        spin_ampl_model = float(sysargv[2])
        flag_bavera = int(sysargv[3])
        #sigma_spin = float(sysargv[4])
        #alpha = float(sysargv[5])
        #sn_model = str(sysargv[6])

    astro_param = {"co_type": co_type,
                   "spin_ampl_model": spin_ampl_model,
                   "flag_bavera": flag_bavera,
#                   "mag_spin": sigma_spin,
#                   "alpha": alpha,
#                   "sn_model": sn_model
                  }


    # Name of cosmoRate directory path (has to be filled only for process_astro_model.py)
#    dir_cosmo_rate = "input_files_cosmorate"  # path toward cosmorate directory
    # WARNING catalogues are going to be overwritten!!!!!!
    if int(sysargv[3])== 1 :
        bavera = '_conBavera'
    else :
        bavera = ''  
    dir_cosmo_rate = "/home/perigois/Downloads/Rufolo_2021/output_sigma03/output_spinflag"+str(sysargv[2])+bavera+'/BBHs'

    # Compact object parameters
    co_param = ["Mc", "q", "z", 'chieff','chip']
    #co_param = ["Mc", "q", "z"]

    # Setup the spin model
    mag_gen_param = {"name_dist": "Maxwellian", "parameters": {"sigma": 0.1}}
    name_spin_model = "Isolated_tilt"

    return dir_cosmo_rate, astro_param, co_param, mag_gen_param, name_spin_model
