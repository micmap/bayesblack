from scipy.stats import maxwell
from schema import Schema, SchemaError
import numpy as np
from Project_Modules.utility_functions import cos_theta_isolated_tilt, comp_chieff, comp_chip, SchemaErrorVerbose, \
    hierarchical_spin, trimmed_gen_spin, gen_spin_bhns


class SpinModel:
    """This class represents a fiducial spin model.
    """

    available_spin_models = ["Aligned", "Isotropic", "Isolated_tilt", "Isotropic_Hierarchical",
                             "Model_BHNS_iso", "Model_BHNS_dyn"]

    mag_distribution_available = ["Maxwellian"]
    mag_gen_param_schema = Schema({"name_dist": str, "parameters": dict})

    def __init__(self, name_model, mag_gen_param=None):
        """Initiates an instance for a SpinModel.

        Parameters
        ----------
        name_model : str
            Name of the spin model
        mag_gen_param : dict
            Dictionary containing the name and parameters for the distribution used to generate chi1 and chi2
            (default = None)
        """

        # Name of spin models
        if name_model not in self.available_spin_models:
            raise ValueError(f"Spin model {name_model} not available. Choose in list of current models: "
                             f"{self.available_spin_models}")
        self.name_model = name_model

        # Magnitude spin generator
        mag_gen = None
        if mag_gen_param is not None:

            # Check if mag_gen_opt has the good form
            try:
                assert self.mag_gen_param_schema.validate(mag_gen_param)
            except SchemaError:
                raise SchemaErrorVerbose("mag_gen_opt", self.mag_gen_param_schema)

            # Check if distribution is available
            if mag_gen_param["name_dist"] not in self.mag_distribution_available:
                raise ValueError(f"Magnitude distribution model {mag_gen_param['name_dist']} not available. Choose in "
                                 f"{self.mag_distribution_available})")
            self.mag_gen_param = mag_gen_param

            # Create magnitude generator
            mag_gen = self.create_magnitude_generator()
        self.mag_gen = mag_gen

    def create_magnitude_generator(self):
        """This function generates a distribution to use to generate chi1 and chi2 from a give distribution.

        Returns
        -------
        distribution : scipy stats distribution
            Distribution that can be used to generate the values for chi1 and chi2
        """
        distribution = None
        if self.mag_gen_param["name_dist"] == "Maxwellian":
            param_schema = Schema({"sigma": float})
            assert param_schema.validate(self.mag_gen_param["parameters"])
            distribution = maxwell(loc=0.0, scale=self.mag_gen_param["parameters"]["sigma"])

        return distribution

    def compute_cos_thetas(self, size, theta1=None, theta2=None, cos_nu1=None, cos_nu2=None):
        """Compute values for the cosine of the angle between the angular momentum and spins for both binary components

        Parameters
        ----------
        size : int
            Legnth of the output
        theta1 : numpy array
            Angle between spin and angular momentum for primary binary components (default = None)
        theta2 : numpy array
            Angle between spin and angular momentum for secondary binary components (default = None)
        cos_nu1 : numpy array
            Cosine of the angle between the orbital angular momentumvector after and before a SN explosion for
            1st component of the binary (default = None)
        cos_nu2 : numpy array
            Cosine of the angle between the orbital angular momentumvector after and before a SN explosion for
            2nd component of the binary (default = None)

        Returns
        -------
        cos_theta1 : numpy array
            Value of cos_theta1 that will be used to compute chieff
        cos_theta2 : numpy array
            Value of cos_theta2 that will be used to compute chieff
        """

        # If theta1 and theta2 are provided, directly return the cosine of these angles
        if theta1 is not None or theta2 is not None:
            if len(theta1) != len(theta2):
                raise IndexError("Size of theta1 and theta2 are not the same.")
            return np.cos(theta1), np.cos(theta2)

        # Depending on the model chosen, generate the values of cos_theta1 and cos_theta2
        if self.name_model == "Aligned":

            # Equal to one
            cos_theta1 = np.ones(size)
            cos_theta2 = np.ones(size)

            return cos_theta1, cos_theta2
        elif self.name_model in ["Isotropic", "Isotropic_Hierarchical", "Model_BHNS_dyn"]:

            # Generate uniformly between -1 and 1
            cos_theta1 = 2. * np.random.uniform(0.0, 1.0, size) - 1.0
            cos_theta2 = 2. * np.random.uniform(0.0, 1.0, size) - 1.0
            return cos_theta1, cos_theta2

        elif self.name_model in ["Isolated_tilt", "Model_BHNS_iso"]:

            # Check that the inputs have the good format
            if cos_nu1 is None or cos_nu2 is None:
                raise ValueError("Values for cos_nu1 and cos_nu2 must be provided when using model {}."
                                 "".format(self.name_model))
            if len(cos_nu1) != len(cos_nu2):
                raise IndexError("Size of cos_nu1 and cos_nu2 are not the same")

            # In this model, cos_theta1 and cos_theta2 are equal
            cos_theta = cos_theta_isolated_tilt(cos_nu1=cos_nu1, cos_nu2=cos_nu2)
            return cos_theta, cos_theta
        else:
            raise ValueError("Wront model of spin.")

    def compute_chis(self, size, m1=None, m2=None, frac_hier=None):

        if self.name_model in ["Aligned", "Isotropic", "Isolated_tilt"]:
            chi1, chi2 = trimmed_gen_spin(size, self.mag_gen)
        elif self.name_model == "Isotropic_Hierarchical":
            chi1, chi2 = hierarchical_spin(size, self.mag_gen, m1, m2, frac_hier)
        elif self.name_model in ["Model_BHNS_iso", "Model_BHNS_dyn"]:
            chi1, chi2 = gen_spin_bhns(size, self.mag_gen)
        else:
            raise ValueError("No proper model was found to generate chi1 and chi2.")

        return chi1, chi2

    def compute_chieff(self, m1, m2, chi1=None, chi2=None, frac_hier=None, cos_theta1=None, cos_theta2=None):
        """ This function generates values for the effective spin according to the fiducial model selected

        Parameters
        ----------
        m1 : numpy array
            Masses of primary binary components
        m2 : numpy array
            Masses of secondary binary components
        theta1 : numpy array
            Angle between spin and angular momentum for primary binary components (default = None)
        theta2 : numpy array
            Angle between spin and angular momentum for secondary binary components (default = None)
        chi1 : numpy array
            Value for the magnitude of the spin for primary binary component (default = None)
        chi2 : numpy array
            Value for the magnitude of the spin for primary binary component (default = None)
        cos_nu1 : numpy array
            Cosine of the angle between the orbital angular momentumvector after and before a SN explosion for
            1st component of the binary (default = None)
        cos_nu2 : numpy array
            Cosine of the angle between the orbital angular momentumvector after and before a SN explosion for
            2nd component of the binary (default = None)
        frac_hier : float
            Fraction of hierarchical mergers

        Returns
        -------
        chieff : numpy array
            Values for the chi effective for the binary systems.
        """
        # Check that masses are properly defined
        if len(m1) == len(m2):
            size = len(m1)
        else:
            raise IndexError("Size of input arrays m1 and m2 must be the same")

        # If chi1 and chi2 are not provided, generate them using the model for the distribution given in input.
        if chi1 is None or chi2 is None:
            if self.mag_gen is None:
                raise ValueError("Values for chi1 and chi2 were not provided, and no model for the spin magnitude was "
                                 "given in input.")
            chi1, chi2 = self.compute_chis(size, m1=m1, m2=m2, frac_hier=frac_hier)

        else:
            # If chi1 and chi2 are provided, check that they have the same sizes than m1 and m2
            if len(chi1) != size or len(chi2) != size:
                raise IndexError("Size of input arrays chi1 and chi2 must be equal to size of m1 and m2.")

        # Compute chieff
        chieff = comp_chieff(m1, m2, chi1, chi2, cos_theta1, cos_theta2)

        return chieff

    def compute_chip(self, m1, m2, cos_theta1=None, cos_theta2=None, chi1=None, chi2=None, cos_nu1=None, cos_nu2=None,
                       frac_hier=None):
        """ This function generates values for the effective spin according to the fiducial model selected

        Parameters
        ----------
        m1 : numpy array
            Masses of primary binary components
        m2 : numpy array
            Masses of secondary binary components
        theta1 : numpy array
            Angle between spin and angular momentum for primary binary components (default = None)
        theta2 : numpy array
            Angle between spin and angular momentum for secondary binary components (default = None)
        chi1 : numpy array
            Value for the magnitude of the spin for primary binary component (default = None)
        chi2 : numpy array
            Value for the magnitude of the spin for secondary binary component (default = None)
        cos_nu1 : numpy array
            Cosine of the angle between the orbital angular momentumvector after and before a SN explosion for
            1st component of the binary (default = None)
        cos_nu2 : numpy array
            Cosine of the angle between the orbital angular momentumvector after and before a SN explosion for
            2nd component of the binary (default = None)
        frac_hier : float
            Fraction of hierarchical mergers

        Returns
        -------
        chieff : numpy array
            Values for the chi effective for the binary systems.
        """
        # Check that masses are properly defined
        if len(m1) == len(m2):
            size = len(m1)
        else:
            raise IndexError("Size of input arrays m1 and m2 must be the same")

        # If chi1 and chi2 are not provided, generate them using the model for the distribution given in input.
        if chi1 is None or chi2 is None:
            if self.mag_gen is None:
                raise ValueError("Values for chi1 and chi2 were not provided, and no model for the spin magnitude was "
                                 "given in input.")
            chi1, chi2 = self.compute_chis(size, m1=m1, m2=m2, frac_hier=frac_hier)

        else:
            # If chi1 and chi2 are provided, check that they have the same sizes than m1 and m2
            if len(chi1) != size or len(chi2) != size:
                raise IndexError("Size of input arrays chi1 and chi2 must be equal to size of m1 and m2.")

        # Compute chip
        chip = comp_chip(m1, m2, chi1, chi2, cos_theta1, cos_theta2)

        return chip


class MagGenError(Exception):

    def __init__(self, distribution, mag_distribution_available):
        self.distribution = distribution
        self.mag_distribution_available = mag_distribution_available

    def __str__(self):
        error_string = """
        Magnitude distribution {} not available to sample chi1 and chi2.
        Choose in list of current models: {}""".format(self.distribution, self.mag_distribution_available)
        return error_string


class MagGenType(Exception):

    def __init__(self, mag_gen_opt_schema):
        self.mag_gen_opt_schema = mag_gen_opt_schema

    def __str__(self):
        error_string = """\n
        The format of the magnitude generator dictionary is not respected.
        Make sure that the format is :\n
        {}""".format(self.mag_gen_opt_schema)
        return error_string
