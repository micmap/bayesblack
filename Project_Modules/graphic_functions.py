
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import corner as cr


def corner(data, title, var_select=None, save=False, quantiles=None):
    """Corner plot for selected parameters. It uses the package corner.py, with minimum functionnality as
    some features seem to need some fixing.

    Parameters
    ----------
    data : pandas dataframe
        Data to use for plotting
    title : str
        Title for the plot, also used to name the file if save is active
    var_select : list of str
        List of variables considered for the corner plot. If None, use loaded instance variables (default = None)
    save : bool
        If True, save the figure.
    quantiles : list of float
        List of quantiles that appear as lines in 1d-histograms of the corner plot.
    """

    # Set the values for quantiles
    if quantiles is None:
        quantiles = [0.05, 0.5, 0.95]
    else:
        if type(quantiles) != list:
            raise TypeError("Quantiles must be a list.")

    # Use all variables if None
    if var_select is None:
        var_select = data.columns

    # Define labels
    labels = list(data.columns)

    # Do the corner plot using corner.py
    # Default 2d contours are shown at 0.5, 1, 1.5, and 2 sigma.
    cr.corner(data[var_select], labels=labels, quantiles=quantiles)

    # If specified, save the figure
    if save:
        plt.savefig(title)

    # Show the figure
    plt.show()


def hist_1d(data, var, ax=None, bins=50, title=False, logx=False, logy=False, range_x=None, save=False,
            namefile=None, show=True):
    """Do a 1d-histogram for the specified variable.

    Parameters
    ----------
    data : pandas dataframe
        Data to use for plotting
    var : str
        Name of 1st variable
    title : bool
        If True, add a title to the graph
    ax : matplotlib.axes.Axes object
        If specified, use the axis to plot the figure (multiple plots). If None, create a new figure
        (default = None)
    bins : int
        Number of bins to use for the plot (default = 50)
    logx : bool
        If True, set the x-axis logarithmic (default = False)
    logy : bool
        If True, set the y-axis logarithmic (default = False)
    range_x : tuple
        If specified, use this range for y-axis. Need to also set range_y at the same time (default = None)
    save : bool
        If True, save the figure (default = False)
    namefile: str
        Name of the file to save if save was set to true (default = None)
    show : bool
        If true, disply the graph (default = True)
    """

    # Check that variable is well defined
    if type(var) != str:
        raise TypeError("The variable selected must be a string")
    if var not in list(data.columns):
        raise ValueError(f"{var} not in parameters loaded for this event, ({list(data.columns)})")

    # If ax is specified use it to plot, otherwise create a new figure
    if ax is None:
        plt.figure(figsize=(12, 8))
        ax = plt.gca()
    else:
        if not isinstance(ax, Axes):
            raise TypeError("ax must be a Matplotlib Axes object.")

    # Check that range_x is well defined if specified
    if range_x is not None:
        if type(range_x) != tuple:
            TypeError("The range set to do the histograms must be given as a tuple.")

    # Set the x/y-axis to be logarithmic if specified
    if logx:
        ax.set_xscale("log")
    if logy:
        ax.set_yscale("log")

    # Plot the histogram for the data
    ax.hist(data[var], histtype="step", lw=3, bins=bins, range=range_x, density=True)

    # Set labels for axis and legends
    ax.set_xlabel(var, fontsize=23)
    ax.set_ylabel("p(" + var + ")", fontsize=23)
    ax.tick_params(axis='x', which=u'major', labelsize=12, length=10)
    ax.tick_params(axis='x', which=u'minor', labelsize=12, length=7)
    ax.tick_params(axis='y', which=u'major', labelsize=12, length=10)
    ax.tick_params(axis='y', which=u'minor', labelsize=12, length=7)

    if title:
        title = "Histogram_" + "_" + var
        ax.set_title(title, fontsize=30)

    # Save figure if option selected
    if save:
        if namefile is None or type(namefile) != str:
            namefile = "Histogram_" + "_" + var
        plt.savefig(namefile)

    # Show the plot
    if show:
        plt.show()


def hist_2d(data, var1, var2, ax=None, bins=50, title=None, logx=False, logy=False, range_x=None, range_y=None,
            save=False, namefile=None, show=True):
    """Do a 2d-histogram for the specified variables of data.

    Parameters
    ----------
    data : pandas dataframe
        Data to use for plotting
    var1 : str
        Name of 1st variable
    var2 : str
        Name of 2nd variable
    title : str
        Title for the plot, also used to name the file if save is active
    ax : matplotlib.axes.Axes object
        If specified, use the axis to plot the figure (multiple plots). If None, create a new figure
        (default = None)
    bins : int
        Number of bins to use for the plot (default = 50)
    logx : bool
        If True, set the x-axis logarithmic (default = False)
    logy : bool
        If True, set the y-axis logarithmic (default = False)
    range_x : tuple
        If specified, use this range for y-axis. Need to also set range_y at the same time (default = None)
    range_y : tuple
        If specified, use this range for y-axis. Need to also set range_x at the same time (default = None)
    save : bool
        If True, save the figure (default = False)
    namefile: str
        Name of the file to save if save was set to true (default = None)
    show : bool
        If true, disply the graph (default = True)
    """

    # Check that variable names are ok
    if var1 not in list(data.columns):
        raise ValueError(f"{var1} not in parameters loaded for this event, ({list(data.columns)})")
    if var2 not in list(data.columns):
        raise ValueError(f"{var2} not in parameters loaded for this event, ({list(data.columns)})")

    # If ax is specified use it to plot, otherwise create a new figure
    if ax is None:
        plt.figure(figsize=(12, 8))
        ax = plt.gca()
    else:
        if not isinstance(ax, Axes):
            raise TypeError("ax must be a Matplotlib Axes object.")

    # Set the range for the x-axis
    if range_x is not None and range_y is not None:
        if range_x is None or range_y is None:
            raise ValueError("Both range_x and range_y must be selected in this case.")
        if type(range_x) != tuple or type(range_y) != tuple:
            TypeError("range_x and range_y must be tuples.")
        range_2d = [[range_x[0], range_x[1]], [range_y[0], range_y[1]]]
    else:
        range_2d = None

    # Plot the 2d histogram
    ax.hist2d(data[var1], data[var2], bins=bins, range=range_2d, density=True)

    # Set the label and ticks
    ax.set_xlabel(var1, fontsize=23)
    ax.set_ylabel(var2, fontsize=23)
    ax.tick_params(axis='x', which=u'major', labelsize=12, length=10)
    ax.tick_params(axis='x', which=u'minor', labelsize=12, length=7)
    ax.tick_params(axis='y', which=u'major', labelsize=12, length=10)
    ax.tick_params(axis='y', which=u'minor', labelsize=12, length=7)

    if title is None or type(title) != str:
        title = "Histogram_" + "_" + var1 + "_" + var2
    ax.set_title(title, fontsize=30)

    # Set the x/y-axis to be logarithmic if specified
    if logx:
        ax.set_xscale("log")
    if logy:
        ax.set_yscale("log")

    # If specified, save the figure
    if save:
        if namefile is None or type(namefile) != str:
            namefile = "Histogram_" + "_" + var1 + "_" + var2
        plt.savefig(namefile)
    if save:
        plt.savefig(title)

    # Show the figure
    if show:
        plt.show()
