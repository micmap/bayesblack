import os.path
import pandas as pd
import numpy as np
from astropy.cosmology import Planck15
import astropy.units as u
import random
from Project_Modules.auxiliary_cosmorate import LogFileCR, regex_catalog
from Project_Modules.utility_functions import m1_m2_to_mc_q, clean_path, SchemaErrorVerbose, \
    flatten_restrict_range_output_emcee, check_inputlist_with_accessible_values
from Project_Modules import graphic_functions as gf
from Project_Modules.spin_model import SpinModel
from schema import Schema, SchemaError
import concurrent.futures
import emcee
import re
import scipy.stats
import matplotlib.pyplot as plt
from matplotlib.axes import Axes


class AstroModel:
    """Class that corresponds to a log-file associated with CosmoRate pre-processing.

    Attributes
    ----------
    loaded_flag : dict
        Dictionary indicating what data are loaded (catalog, merger rate, spin model)
    astro_model_parameters : dict
        Dictionary where keys are the parameters of the AstroModel instance with the associated values
    co_parameters : list of str
        List of compact object parameters used to define AstroModel
    name_model : str
        String characterizing the AstroModel instance and made from parameters name and values. This name
        is used to reference any file connected to the model instance.
    spin_model : object SpinModel
        Spin model object used for this instance of AstroModel
    dir_cat : str
        Path towards directory where catalog file is stored
    dir_mrd : str
        Path towards directory where merger rate density file is stored

    Methods
    -------
    check_astro_model_parameters(astro_model_parameters)
        Check that the parameters of the astrophysical model are conform
    check_co_parameters(co_parameters)
        Check that the parameters of the compact objects are conform
    read_merger_rate_file(dir_mrd=None, delimiter="\t")
        Read the merger rate file.
    create_merger_rate_file(dir_mrd_cosmorate, name_mrd_cosmorate, delimiter="\t")
        Construct the merger rate file from CosmoRate sourcefiles.
    read_catalog_file(dir_cat=None, delimiter="\t")
        Read the catalog file.
    create_catalog_file(dir_model_cosmorate, identifier_file, num_cat, range_z, overwrite=False, delimiter="\t")
        Construct the catalog file from CosmoRate sourcefiles.
    sample_catalog(n_walkers, n_chain, bw_method, n_cpu)
        Generate samples from catalog using MCMC.
    sample_catalog_one_cpu(args)
        1-CPU routine sampling routine called from sample_catalog
    """

    astro_model_parameters_available = {"co_type": ["BBH", "BNS", "BHNS"],
                                        #"formation_channel": ["Iso", "YSC", "NSC", "GC"],
                                        #"sigma_logmetallicity": [0.2, 0.3, 0.4],
                                        #"mag_spin": [0.1, 0.01],
                                        #"alpha": [0.5, 1.0, 2.0, 3.0, 5.0, 7.0, 10.0]} #,
                                        #"sn_model": ["delayed", "rapid"]
                                        "spin_ampl_model" : [1, 2, 3, 4],
					                    "flag_bavera" : [0, 1]
                                        }

    astro_model_schema = Schema({"co_type": str,
                                 #"formation_channel": str,
                                 #"sigma_logmetallicity": float,
                                 #"mag_spin": float,
                                 #"alpha": float}) #,
                                 #"sn_model": str"
			                     "spin_ampl_model" : float,
				                 "flag_bavera" : int
                                 })

    map_name_par = {"co_type": "co",
                    #"formation_channel": "fc",
                    #"sigma_logmetallicity": "logmet",
                    #"mag_spin": "magsp",
                    #"alpha": "a",
		            "spin_ampl_model" : "sam",
		            "flag_bavera" : "Bav"
                    }
                    
#---------------------------------------------------------------------------------------------------------

    co_parameters_available = ["m1", "m2", "Mc", "q", "chieff", "z", "Dl", "chip"]


    def __init__(self, astro_model_parameters, co_parameters, spin_model=None, load_cat=True, load_mrd=True,
                 dir_cat="Astro_Models/Catalogs/", dir_mrd="Astro_Models/MergerRateDensity/"):
        """Create an instance of AstroModel setting the various astro / compact object parameters given in
        input, and loading various data (catlogs / merger rate density) if specified.

        Parameters
        ----------
        astro_model_parameters : dict
            Dictionary where keys are the parameters of the AstroModel instance with the associated values
        co_parameters : list of str
            List of compact object parameters used to define AstroModel
        spin_model : object SpinModel
            Spin model object used for this instance of AstroModel (default = None)
        load_cat : boolean
            True if catalog must be loaded during instanciation of AstroModel, False otherwise (default = True)
        load_mrd : boolean
            True if merger rate density must be loaded during instanciation of AstroModel, False otherwise
            (default = True)
        dir_cat : string
            Path towards directory where catalog file is stored (default = "Astro_Models/Catalogs/")
        dir_mrd : string
            Path towards directory where merger rate density file is stored (default =
            "Astro_Models/MergerRateDensity/")
        """

        # Flags for loading data
        self.loaded_flag = {"cat": False, "mrd": False, "spin_model": False}

        # Astro model parameters: check and assign
        self.check_astro_model_parameters(astro_model_parameters)
        self.astro_model_parameters = astro_model_parameters
        self.name_model = "_".join([self.map_name_par[x] + "_" + str(self.astro_model_parameters[x])
                                    for x in self.astro_model_parameters]) + ".dat"

        # Spin model: check and assign if applicable
        if spin_model is not None:
            if isinstance(spin_model, SpinModel):
                self.loaded_flag["spin_model"] = True
            else:
                raise TypeError("\nError: spin_model is not of expected type.\n"
                                "Please pass an instance of class SpinModel() in input.")
        self.spin_model = spin_model

        # Compact object parameters: check and assign
        self.co_parameters_available = self.co_parameters_available[:]
        self.check_co_parameters(co_parameters)
        self.co_parameters = co_parameters

        # Set paths for catalogs and merger rate density. Create directories if needed
        self.dir_cat = clean_path(dir_cat)
        if not os.path.exists(self.dir_cat):
            os.makedirs(self.dir_cat)
        self.dir_mrd = clean_path(dir_mrd)
        if not os.path.exists(self.dir_mrd):
            os.makedirs(self.dir_mrd)

        # Load catalogs if option set to True
        self.data_cat = None
        if load_cat:
            self.read_catalog_file()

        # Load merger rate density if option set to True
        self.data_mrd = None
        if load_mrd:
            self.read_merger_rate_file()

    def check_astro_model_parameters(self, astro_model_parameters):
        """Check if astro_model_parameters respect the structure specified by astro_model_parameters_available. It
        also checks that 'co_type' is present.

        Parameters
        ----------
        astro_model_parameters : dict
            Dictionary where keys are the parameters of the AstroModel instance with the associated values
        """

        # Check the input structure of astro_model_parameters
        try:
            assert self.astro_model_schema.validate(astro_model_parameters)
        except SchemaError:
            raise SchemaErrorVerbose("astro_model_parameters", self.astro_model_schema)

        # Check that the compact object type is given
        if "co_type" not in astro_model_parameters:
            raise KeyError("'co_type' has to be given in the model parameters.")

        # Check if astro_model_parameters are in astro_model_parameters_available
        for key in astro_model_parameters:
            if astro_model_parameters[key] not in self.astro_model_parameters_available[key]:
                raise ValueError("The value for parameter {} of astro model must be selected in the set {}"
                                 "".format(key, self.astro_model_parameters_available[key]))

    def check_co_parameters(self, co_parameters):
        """Check if co_parameters respect the structure specified by co_parameters_available.

        Parameters
        ----------
        co_parameters : list of str
            List of compact object parameters used to define AstroModel
        """

        # Check both type and accessible values
        check_inputlist_with_accessible_values(co_parameters, "co_parameters", self.co_parameters_available,
                                               "co_parameters_available")


    def read_merger_rate_file(self, dir_mrd=None, delimiter="\t"):
        """Read the merger rate file and stores data.

        Parameters
        ----------
        dir_mrd : str
            If specified, function will look in this directory to read merger rate file, instead of reading in
            path set during instanciation (default = None)
        delimiter : str
            Delimiter used to separate columns in merger rate file (default = "\t")
        """

        # Set path and namefiles
        if dir_mrd is not None:
            path = clean_path(dir_mrd)
        else:
            path = self.dir_mrd
        file_mrd = path + "Mrd_" + self.name_model

        # Check file existence
        if not os.path.isfile(file_mrd):
            raise FileNotFoundError("\nThe file for the merger rate density of the model could not be found.\n"
                                    "\t 1) If the merger rate file was not created, run method "
                                    "create_merger_rate_file()\n"
                                    "\t 2) If the merger rate file was created, check that the file "
                                    "is located at {}".format(file_mrd))

        # Read file
        data_mrd = pd.read_csv(file_mrd, delimiter=delimiter)

        # Update instance variables
        self.loaded_flag["mrd"] = True
        self.dir_mrd = path
        self.data_mrd = data_mrd

    def create_merger_rate_file(self, dir_cosmorate, range_z, delimiter="\t"):
        """Create the merger rate file using the information provided by CosmoRate. In current version, the file
        has only with two columns : redshift and merger rate density source frame; and the file is found by
        matching the regex 'MRD'.
        To compute the merger rate, we currently make use of Planck15 cosmology.

        Parameters
        ----------
        dir_cosmorate : str
            Path towards the location of the merger rate file of CosmoRate
        range_z : float
            Maximum range of redshift for the model. Must be a multiple of CosmoRate redshift division
        delimiter : str
            Delimiter used to separate columns in merger rate file (default = "\t")
        """

        # Make sure that the directory ends with /
        dir_cosmorate = clean_path(dir_cosmorate)

        # Search for merger rate file by filtering name with "MRD". It should work if catalogs files have been moved.
        file_name = list(filter(lambda v: re.match('MRD', v), os.listdir(dir_cosmorate)))[0]
        path_file = dir_cosmorate + file_name
        if not os.path.isfile(path_file):
            raise FileNotFoundError("\nThe source file for the merger rate density of CosmoRate could not be found.\n"
                                    "Check that the file is in {} and that there is only one file containing the "
                                    "string 'MRD'".format(dir_cosmorate))

        # Read file
        data_original = np.loadtxt(path_file, skiprows=1)

        # Check that range_z is a multiple of deltaz of cosmoRate
        redshift = data_original[:, 0]
        delta_z = round(redshift[1] - redshift[0], 5)
        if not (range_z / delta_z).is_integer():
            raise ValueError("Error: the redshift range {} is not a multiple of the redshift interval {} from "
                             "CosmoRate.".format(range_z, delta_z))

        # Compute merger rate density and merger rate in detector-frame. Planck 15 cosmology is used here
        mrd_source_frame = data_original[:, 1]
        mrd_detector_frame = np.array([mrd_s * (1.0/(1.0 + z)) for mrd_s, z in zip(mrd_source_frame, redshift)])
        dvc_dz = np.array([4. * np.pi * Planck15.differential_comoving_volume(z).to(u.Gpc ** 3 / u.sr).value
                           for z in redshift])
        mr_detector_frame = np.array([dvc * mr_df for dvc, mr_df in zip(dvc_dz, mrd_detector_frame)])

        # Create and write file.
        with open(self.dir_mrd + "Mrd_" + self.name_model, "w") as fileout:
            fileout.write(delimiter.join(["z", "mrd_sf", "mrd_df", "mr_df"]) + "\n")
            for z, mrd_sf, mrd_df, mr_df in zip(redshift, mrd_source_frame, mrd_detector_frame, mr_detector_frame):
                if z <= range_z:
                    fileout.write("{0:.4f} {4} {1:.4f} {4} {2:.4f} {4} {3:.4f} \n"
                              "".format(z, mrd_sf, mrd_df, mr_df, delimiter))
                else:
                    return

    def read_catalog_file(self, dir_cat=None, delimiter="\t"):
        """Read the catalog file and stores data.

        Parameters
        ----------
        dir_cat : str
            If specified, function will look in this directory to read catalog file, instead of reading in path
            set during instanciation (default = None)
        delimiter : str
            Delimiter used to separate columns in catalog file (default = "\t")
        """

        # Set path and namefiles
        if dir_cat is not None:
            path = clean_path(dir_cat)
        else:
            path = self.dir_cat
        file_cat = path + "Catalog_" + self.name_model

        # Check file existence
        if not os.path.isfile(file_cat):
            raise FileNotFoundError("\nThe file for the model's catalog could not be found.\n"
                                    "\t 1) If the catalog was not created, run method "
                                    "create_catalog_file()\n"
                                    "\t 2) If the catalogfile was created, check that the file "
                                    "is located at {}".format(file_cat))

        # Read catalog
        df = pd.read_csv(file_cat, delimiter=delimiter)
        if any(x not in df.columns for x in self.co_parameters):
            raise KeyError("One of the parameter you are trying to access is not present in the catalog. This results "
                           "from the fact that create_catalog_file was run with a different set of parameters.\n. "
                           "Please re-run create_catalog_file() with the set {}".format(self.co_parameters))
        data_cat = df[self.co_parameters]

        # Update instance variables
        self.loaded_flag["cat"] = True
        self.dir_cat = path
        self.data_cat = data_cat

    def create_catalog_file(self, dir_cosmorate, num_cat, frac_hier=None, overwrite=False,
                            delimiter="\t"):
        """Create the catalog file using the information from CosmoRate and previously created merger rate
        density file.
        In current version, CosmoRate files have the name "identifier_file + "_" + str(i) + "_50.dat" where i
        refers to each redshift bin. It this changes, the code below needs to be updated.

        Parameters
        ----------
        dir_cosmorate : str
            Path towards the location of the catalog file of CosmoRate
        num_cat : int
            Number of sources wanted for the catalog
        frac_hier : float
            Fraction of hierarchical mergers (used for specific spin model)
        overwrite : bool
            If set to True, erase and rewrite the catalog if the catalog already existed (default = False)
        delimiter : str
            Delimiter used to separate columns in catalog file (default = "\t")
        """

        # Set name of catalog file
        cat_file = clean_path(self.dir_cat) + "Catalog_" + self.name_model
        try:
            assert not os.path.isfile(cat_file) or overwrite
        except AssertionError:
            print("Catalog '{}' already exists and hence was not created.\n"
                  "If you want to overwrite the catalog, run 'create_catalog_file' with argument 'overwrite' set "
                  "to True".format(cat_file))
            return

        # Check if CosmoRate files were processed before by looking at log-file
        dir_cosmorate = clean_path(dir_cosmorate)
        log_file = LogFileCR(dir_cosmorate)
        if not log_file.status["header_rewritten"]:
            raise ValueError("Ran CosmoRate pre-processing routines before creating catalog file.")

        # Read merger file if it was not done before
        if not self.loaded_flag["mrd"]:
            self.read_merger_rate_file()

        # Read merger rate, and create a cumulative sum from it. Then randomly generate points in [0,1] and use
        # cdf values as bins. The number that fall in each bin, is then the number of sources associated with
        # the redshift bin
        mr_df = self.data_mrd['mr_df']
        cdf = np.append(0.0, np.cumsum(mr_df/mr_df.sum()))
        counts, bin_edges = np.histogram(np.random.rand(num_cat), bins=cdf)

        # Initiate dataframe that will contains catalog values
        df_final = pd.DataFrame(columns=self.co_parameters)

        # Get the names of catalog files from CosmoRate
        dir_catfile = dir_cosmorate + "/catalogs/"
        match_cat = re.match(regex_catalog, os.listdir(dir_catfile)[0])
        match_beg, match_end = match_cat.groups()

        print("*******  START : CATALOG CREATION  *******")

        # Loop over redshift bins
        for i, c in enumerate(counts):

            # Read CosmoRate catalog. So far the name of catalog files is  "identifier_file_i_50.dat"
            # If CosmoRate changes, updates this part too.

            ##cat_source_name = dir_catfile + match_beg + "_" + str(i + 1) + "_" + match_end + ".dat"
            cat_source_name = dir_catfile + match_beg + "_" + str(i + 1)  + ".dat"
            cat_source_name = dir_cosmorate + 'catalogs/BBHs_spin2020_'+str(i+1)+'_50.dat'

            df = pd.read_csv(cat_source_name, delimiter="\t")
            size = len(df["m1"])
            if "th1" not in df.columns :
                df["cos_th1"], df["cos_th2"] = self.spin_model.compute_cos_thetas(size,  cos_nu1=df.get("cos_nu1"),cos_nu2=df.get("cos_nu2"))
            else :
                df["cos_th1"], df["cos_th2"] = self.spin_model.compute_cos_thetas(size, theta1=df.get("th1"),theta2=df.get("th2"))
            # Map to Mc, q if they are selected as parameters
            if "Mc" in self.co_parameters or "q" in self.co_parameters:
                df["Mc"], df["q"] = m1_m2_to_mc_q(df["m1"], df["m2"])

            # Compute chi_effective
            if "chieff" in self.co_parameters:
                if not self.loaded_flag["spin_model"]:
                    raise ValueError("No correct spin_model was loaded to compute spin-related quantities.")
                df['chieff'] = self.spin_model.compute_chieff(df["m1"], df["m2"], cos_theta1=df.get("cos_th1"),
                                                              cos_theta2=df.get("cos_th2"), chi1=df.get("chi1"),
                                                              chi2=df.get("chi2"), frac_hier=frac_hier)

            # Compute luminosity distance if not already in CosmoRate
            if "Dl" in self.co_parameters and "Dl" not in df.columns:
                df["Dl"] = Planck15.luminosity_distance(df["z"]).value

            if "chip" in self.co_parameters and "chip" not in df.columns :
                df["chip"] = self.spin_model.compute_chip(df["m1"], df["m2"], cos_theta1=df.get("cos_th1"),
                                                              cos_theta2=df.get("cos_th2"), chi1=df.get("chi1"),
                                                              chi2=df.get("chi2"), frac_hier=frac_hier)

            #  Select only relevant parameters
            df = df[self.co_parameters]

            # Concatenate dataframe with correct number of elements
            for _ in range(int(c/len(df))):
                df_final = pd.concat([df_final, df])
            ind_list = random.sample(range(len(df)), c % len(df))
            df_final = pd.concat([df_final, df.iloc[ind_list]])
            print(f"Redshift bin {i+1}, current aggregated length dataframe {df_final.shape[0]}")

        # Write dataframe to file
        df_final.to_csv(cat_file, sep=delimiter, index=False, float_format="%.4f")
        print("*******  END : CATALOG CREATION  *******")

    def sample_catalog(self, n_walkers, n_chain, bw_method, n_cpu):
        """Routine to generate samples from the catalogs of the astrophysical model. In particular, this is used
        to compute the efficiency of the model (or VT).
        The samples are generated using MCMC with emcee. In current version, the entire MCMC chain is exported
        as samples, implying some correlation left in the samples.

        Parameters
        ----------
        n_walkers : int
            Number of walkers for the MCMC chain, must be superior to twice the dimension of the problem at hand.
        n_chain : int
            Length of the chain generated.
        bw_method : float or str
            Bandwidth method used for kernel density estimation.
        n_cpu : int
            Number of CPU selected to run the routine.

        Returns
        -------
        samples : 'pandas dataframe'
            Samples generated by the routine
        """

        # Create arguments to be set to multiple CPUs
        args = ((n_walkers, n_chain, bw_method, cpu) for cpu in range(n_cpu))

        # Print a warning that user should dismiss the warning error from emcee
        print("WARNING : Do not pay attention of the warning from the infinite with the "
              "log. This is due to emcee, and is not affecting the end computation.")
        # Main function
        if n_cpu > 1:  # Parallel option

            # Parallel computation
            with concurrent.futures.ProcessPoolExecutor() as executor:
                results = executor.map(self.sample_catalog_one_cpu, args)

            # Concatenate the samples from the various CPUs
            samples_final = pd.DataFrame()
            for df in results:
                if df is not None:
                    samples_final = samples_final.append(df)
            samples_final = samples_final.reset_index(drop=True)

        else:  # Single CPU
            samples_final = self.sample_catalog_one_cpu(*args)

        return samples_final

    def sample_catalog_one_cpu(self, args):
        """This function generates samples for one CPU.

        Parameters
        ----------
        args : tuple
            This tuple contains the same argument than the ones of sample_catalog. This tuple format is used to
            make concurrent futures work.

        Returns
        -------
        samples : pandas dataframe
            Samples generated by this CPU.
        """

        n_walkers, n_chain, bw_method, cpu = args
        n_dim = len(self.co_parameters)
        print(f"cpu {cpu}, args {args}")

        # Different seeds for each CPU
        np.random.seed()

        # Compute KDE
        kde = scipy.stats.gaussian_kde(np.array([self.data_cat[x] for x in self.co_parameters]),
                                       bw_method=bw_method)

        # No idea why, but log_prob does not work on scighera
        def log_prob(x, kde_in):
            return np.log(kde_in(x))

        # Draw a random point in the range set by the 99% credible interval
        min_quantile = np.array(self.data_cat.quantile(0.005))
        max_quantile = np.array(self.data_cat.quantile(0.995))
        p0 = min_quantile + (max_quantile - min_quantile) * np.random.rand(n_walkers, n_dim)

        # Define sampler
        sampler = emcee.EnsembleSampler(n_walkers, n_dim, log_prob, args=[kde])

        # Burn-in
        state_ini = sampler.run_mcmc(p0, 50)
        sampler.reset()

        # Main chain
        sampler.run_mcmc(state_ini, n_chain, progress=True)
        #sampler.run_mcmc(state_ini, n_chain)

        # Flatten and restrict chains in the min/max range
        min_np = np.array(self.data_cat.min())
        max_np = np.array(self.data_cat.max())
        samples = flatten_restrict_range_output_emcee(sampler, self.co_parameters, min_np, max_np)
        return samples

    def hist(self, var, ax=None, bins=50, logx=False, logy=False, range_x=None, range_y=None,
             save=False, namefile=None, show=True):
        """Histogram routine for the event parameter. Either do a 1d or 2d histograms depending on inputs.

        Parameters
        ----------
        var : str or list of str
            Name of variable(s)
        ax : matplotlib.axes.Axes object
            If specified, use the axis to plot the figure (multiple plots). If None, create a new figure
            (default = None)
        bins : int
            Number of bins to use for the plot (default = 50)
        logx : bool
            If True, set the x-axis logarithmic (default = False)
        logy : bool
            If True, set the y-axis logarithmic (default = False)
        range_x : tuple
            If specified, use this range for y-axis. (default = None)
        range_y : tuple
            If specified, use this range for y-axis in the 2d case. Need to also set range_x at the same time
            (default = None)
        save : bool
            If True, save the figure  (default = False)
        namefile: str
            Name of the file to save if save was set to true (default = None)
        show : bool
            If true, disply the graph (default = True)
        """

        # Load posterior or prior data
        if not self.loaded_flag["cat"]:
            raise ValueError("Catalog data are not loaded")

        if type(var) == str:  # 1d histogram
            title = None
            gf.hist_1d(self.data_cat, var, ax=ax, bins=bins, title=title, logx=logx, logy=logy,
                       range_x=range_x, save=save, namefile=namefile, show=show)
        elif type(var) == list and len(var) == 1:  # 1d histogram
            title = None
            gf.hist_1d(self.data_cat, var[0], ax=ax, bins=bins, title=title, logx=logx, logy=logy,
                       range_x=range_x, save=save, namefile=namefile, show=show)
        elif type(var) == list and len(var) == 2:  # 2d histograms
            title = None
            gf.hist_2d(self.data_cat, var[0], var[1], ax=ax, bins=bins, title=title, logx=logx, logy=logy,
                       range_x=range_x, range_y=range_y, save=save, namefile=namefile, show=show)
        else:
            raise NotImplementedError("Option not implemented. Use corner() for such set of variables.")

    def corner(self, var_select=None, save=False, quantiles=None):
        """Corner plot for catalog variable. It uses the package corner.py, with minimum functionnality as
        some features seem to need some fixing.

        Parameters
        ----------
        var_select : list of str
            List of variables considered for the corner plot. If None, use loaded instance variables
            (default = None)
        save : bool
            If True, save the figure.
        quantiles : list of float
            List of quantiles that appear as lines in 1d-histograms of the corner plot.
        """

        if not self.loaded_flag["cat"]:
            raise ValueError("Catalog data are not loaded.")
        data = self.data_cat

        # Select the appropriate variables
        if var_select is not None:
            check_inputlist_with_accessible_values(var_select, "var_select", self.co_parameters, "event_par")
        else:
            var_select = self.co_parameters

        title = "CornerPlot_" + "".join(var_select) + "_" + self.name_model
        gf.corner(data, title, var_select=var_select, save=save, quantiles=quantiles)

    def check_sample_hist(self, name_file_samples, var, ax=None, range_x=None, logy=False):

        # Check that catalog is indeed loaded
        if not self.loaded_flag["cat"]:
            raise ValueError("Catalog data are not loaded.")

        # Read samples
        if os.path.isfile(name_file_samples):
            raise FileNotFoundError(f"File {name_file_samples} could not be found")
        data_sample = pd.read_csv(name_file_samples, delimiter="\t")

        # Set axis for the plot
        if ax is None:
            plt.figure(figsize=(12, 8))
            ax = plt.gca()
        else:
            if not isinstance(ax, Axes):
                raise TypeError("ax must be a Matplotlib Axes object.")

        # Check variable is accessible in catalog and sample file
        if var not in self.data_cat.columns:
            raise KeyError(f"{var} not in catalog files.")
        if var not in data_sample.columns:
            raise KeyError(f"{var} not in samples.")

        # Do the plot
        self.hist(var=var, ax=ax, bins=50, range_x=range_x, show=False, logy=logy)
        ax.hist(data_sample[var], density=True, lw=3, histtype="step", bins=50, range=range_x)

        # Set the legend
        ax.legend(["Model", "Sample"], fontsize=20)

    def sources_in_tobs_time(self, tobs):
        """This function computes the predicted number of observations for the model in a given observation time.

        Parameters
        ----------
        tobs : float
            Observation time in year.

        Returns
        -------
        n_sources : float
            Number of sources predicted by the model in an obsevation time of tobs years.
        """

        # Check that observatin time is a float
        if type(tobs) != float:
            raise TypeError("Observation time tobs must be a float.")

        # Check that the merger rate density is loaded
        if not self.loaded_flag["mrd"]:
            raise ValueError("Merger rate density is not loaded.")

        # Compute number of sources. Be careful, the range of redshift used will be the one in the merger rate density
        # file
        n_sources = tobs * np.sum(self.data_mrd['mr_df']) * round(self.data_mrd['z'][1] - self.data_mrd['z'][0], 5)

        return n_sources


class DataNotLoaded(Exception):

    def __str__(self):
        return "Error : The catalog data are not currently loaded. Please reload the models using the flag" \
               "data_load=True, or use the method read_catalog_file()"
