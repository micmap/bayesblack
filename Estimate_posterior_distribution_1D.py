import numpy as np
import time
import os
from Project_Modules.astro_model import AstroModel
from Project_Modules.bayes_model import BayesModel
from Project_Modules.observing_run import ObservingRun
from Project_Modules.detector import DetectorGW
from Project_Modules.utility_functions import compute_log_likelihood, indices_closest_neighbors_in_list, \
    linear_interp

os.environ['MKL_NUM_THREADS'] = '1'  # this command prevents Python from multithreading
                                     # (useful especially for Demoblack machine!)

def jump_hyp_mcmc(hyp_current, scale, limits):
    """
    Parameters
    ----------
    hyp_current : float
        Current value of the hyper-parameter
    scale : float
        Value used for the gaussian scale jump
    limits : list
        List where first/second element is the minimum/maximum range allowed for hyper-parameter

    Returns
    ---------
    hyp_jump_chain : float
        Proposed value for the jump
    """
    outside = True
    hyp_jump_chain = None
    while outside:
        hyp_jump_chain = hyp_current + np.random.normal(0.0, scale)
        if limits[0] <= hyp_jump_chain <= limits[1]:
            outside = False

    return hyp_jump_chain


# ----------------------------------      User input and initialisation     ------------------------------------

# Definition of astro-model properties that will remain constant
co_type = "BBH"
sigma_met = 0.2
mag_spin = 0.1
form_channel = "Iso"

# Definition of the accessible values for the hyperparameter for which we want to estimate
# the posterior distribution
hyp_list = [1.0, 3.0, 5.0]
hyp_name = "alpha"

# Compact object parameter
co_param = ["Mc", "q", "z", "chieff"]

# Option for the Bayesian analysis
bayes_opt = "NoRate"

# List observatios / detector
observing_runs_name = ["O1", "O2", "O3a"]
det_name = ["Livingston_O1", "Livingston_O2", "Livingston_O3a"]

# MCMC chain
N_mcmc = 100000  # total stops for the MCMC chain
scale_jump = 0.1  # scale used for the gaussian jump
namefile_MCMC = "MCMC_met_" + str(sigma_met) + "_spin_" + str(mag_spin) + "_" +\
                bayes_opt + ".dat"

# Load all the models (data_load set to 0 to speed up time and because the moel data are not needed for the analysis)
models_dict = {}
observing_runs = {}
for o in observing_runs_name:
    observing_runs[o] = ObservingRun(o)

interp_detection_efficiency = {}
interp_n_sources = {}
interp_integral_match_model = {}

for hyp in hyp_list:
    for o, d in zip(observing_runs_name, det_name):
        astro_model_param = {"co_type": co_type, "formation_channel": form_channel, "sigma_logmetallicity": sigma_met,
                             "mag_spin": mag_spin, "alpha": hyp}
        model_astro = AstroModel(astro_model_param, co_param, load_cat=False, load_mrd=True)
        detector = DetectorGW(d)
        models_dict[(hyp, o)] = BayesModel(model_astro, observing_runs[o], detector, read_match=True, read_eff=True)

# Initialisation
mcmc_chain = np.zeros((N_mcmc, 2))  # initialise chain, last column contains log-likelihood


# -------------------------------------------      Main code       -------------------------------------------------

# -----------------     Starting MCMC point     -----------------


# Generate a value of hyperparameter hyp randomly within the prior ranges
hyp_ini = np.random.uniform(hyp_list[0], hyp_list[-1])

# Compute log-likelihood
log_likelihood_ini = 0.0
for o in observing_runs:

    integral_match_model = 0.0
    n_obs = observing_runs[o].n_det[co_type]

    ind_l = indices_closest_neighbors_in_list(hyp_ini, hyp_list)
    detection_efficiency = linear_interp(hyp_ini, hyp_list[ind_l], hyp_list[ind_l+1],
                                         models_dict[(hyp_list[ind_l], o)].efficiency,
                                         models_dict[(hyp_list[ind_l+1], o)].efficiency)

    n_sources = linear_interp(hyp_ini, hyp_list[ind_l], hyp_list[ind_l + 1],
                                         models_dict[(hyp_list[ind_l], o)].n_sources,
                                         models_dict[(hyp_list[ind_l + 1], o)].n_sources)

    for key in models_dict[(hyp_list[ind_l], o)].match_model:
        integral_match_model += linear_interp(hyp_ini, hyp_list[ind_l], hyp_list[ind_l + 1],
                                         np.log(models_dict[(hyp_list[ind_l], o)].match_model[key]),
                                         np.log(models_dict[(hyp_list[ind_l + 1], o)].match_model[key]))

    log_likelihood_ini += compute_log_likelihood(bayes_opt, integral_match_model, n_obs, n_sources,
                                                detection_efficiency)


# Record the initial point values and assign them to current position of the chain
mcmc_chain[0] = hyp_ini
mcmc_chain[-1] = log_likelihood_ini
hyp_cur = hyp_ini
log_likelihood_cur = log_likelihood_ini

# -----------------     Main chain     -----------------

start = time.perf_counter()
accept = 1
for n in range(N_mcmc - 1):
    if n % 25000 == 0:
        print("Completion : " + str(100.0 * float(n) / float(N_mcmc)) + " %")

    # Jump proposal for mixing fraction
    hyp_jump = jump_hyp_mcmc(hyp_cur, scale_jump, [hyp_list[0], hyp_list[-1]])

    # Compute log-likelihood
    log_likelihood_jump = 0.0
    for o in observing_runs:

        integral_match_model = 0.0
        n_obs = observing_runs[o].n_det[co_type]

        ind_l = indices_closest_neighbors_in_list(hyp_cur, hyp_list)
        detection_efficiency = linear_interp(hyp_cur, hyp_list[ind_l], hyp_list[ind_l + 1],
                                             models_dict[(hyp_list[ind_l], o)].efficiency,
                                             models_dict[(hyp_list[ind_l + 1], o)].efficiency)

        n_sources = linear_interp(hyp_cur, hyp_list[ind_l], hyp_list[ind_l + 1],
                                  models_dict[(hyp_list[ind_l], o)].n_sources,
                                  models_dict[(hyp_list[ind_l + 1], o)].n_sources)

        for key in models_dict[(hyp_list[ind_l], o)].match_model:
            integral_match_model += linear_interp(hyp_cur, hyp_list[ind_l], hyp_list[ind_l + 1],
                                                  np.log(models_dict[(hyp_list[ind_l], o)].match_model[key]),
                                                  np.log(models_dict[(hyp_list[ind_l + 1], o)].match_model[key]))

        log_likelihood_jump += compute_log_likelihood(bayes_opt, integral_match_model, n_obs, n_sources,
                                                     detection_efficiency)

    # Accept jump with probability Metropolis-Hastings ratio
    Hratio = np.exp(log_likelihood_jump - log_likelihood_cur)
    if Hratio > 1:
        hyp_cur = hyp_jump
        log_likelihood_cur = log_likelihood_jump
        accept += 1
    else:
        beta = np.random.uniform()
        if Hratio > beta:
            hyp_cur = hyp_jump
            log_likelihood_cur = log_likelihood_jump
            accept += 1

    # Update chain values
    mcmc_chain[n + 1, 0] = hyp_cur
    mcmc_chain[n + 1, -1] = log_likelihood_cur

# Take 1 out of 200 points to ensure a good independance of the samples
mcmc_chain = mcmc_chain[::200]

# Create the file containing the output
print("Acceptance rate is : {} %".format(100.0*float(accept) / float(N_mcmc)))
finish = time.perf_counter()
print(f'Finished in {round(finish - start, 2)} second(s)')


with open(namefile_MCMC, "w") as fileout:
    fileout.write(hyp_name + "\t+Likelihood+\n")  # header
    np.savetxt(fileout, mcmc_chain, delimiter='\t', fmt='%.4f')
