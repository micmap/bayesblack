# Module imports
import numpy as np
import sys
import os
from Project_Modules.astro_model import AstroModel
from Project_Modules.spin_model import SpinModel
from Project_Modules.bayes_model import BayesModel
from Project_Modules.observing_run import ObservingRun
from Project_Modules.detector import DetectorGW
from astro_model_param import return_astro_param
from Project_Modules.utility_functions import compute_log_likelihood
os.environ['MKL_NUM_THREADS'] = '1'  # this command prevents Python from multithreading
                                     # (useful especially for Demoblack machine!)

# -------------------------------------------      User input       ------------------------------------------------

# Choose the observing / detector pair used to compute likelihood
observing_runs_name = ["O1", "O2", "O3a"]
detectors_name = ["Livingston_O1", "Livingston_O2", "Livingston_O3a"]

# Choose how to estimate the likelihood
bayes_opt = "NoRate"

# -------------------------------------------      Main code       ------------------------------------------------

# Get all the parameters set in astro_model_param.py
_, astro_param, co_param, mag_gen_param, name_spin_model = return_astro_param(sys.argv)

# Initialise the model
spin_model = SpinModel(name_model=name_spin_model, mag_gen_param=mag_gen_param)
astro_model = AstroModel(astro_model_parameters=astro_param, co_parameters=co_param, spin_model=spin_model,
                         load_cat=False, load_mrd=True)

# Computation of likelihood
log_likelihood = 0.0
for obs, det in zip(observing_runs_name, detectors_name):

    # Initialise observing run
    observing_run = ObservingRun(obs, read_data_posterior=False, read_data_prior=False)

    # Initialise detector
    detector = DetectorGW(det)

    # Initialise Bayesian model
    bayes_model = BayesModel(astro_model=astro_model, observing_run=observing_run, detector=detector,
                             read_match=True, read_eff=True)

    # Get relevant terms for computation
    integral_match_model = np.log(np.array(list(bayes_model.match_model.values()))).sum()
    detection_efficiency = bayes_model.efficiency
    n_sources = bayes_model.n_sources
    n_obs = observing_run.n_det[astro_model.astro_model_parameters["co_type"]]

    # Compute log-likelihood
    log_likelihood_obs = compute_log_likelihood(bayes_opt, integral_match_model, n_obs,
                                                n_sources, detection_efficiency)
    log_likelihood += log_likelihood_obs
    print(f"Log-likelihood for observing run {obs} with detector {det} : {log_likelihood_obs}")

print("The value of the total log-likelihood is : ", log_likelihood)
