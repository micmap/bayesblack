import os
import sys
from Project_Modules.auxiliary_cosmorate import process_cosmorate
from Project_Modules.spin_model import SpinModel
from Project_Modules.astro_model import AstroModel
from astro_model_param import return_astro_param
os.environ['MKL_NUM_THREADS'] = '1' # this command prevents Python from multithreading 
                                    #(useful especiallfy for Demoblack machine!)

# --------------------------------------------      User input       ---------------------------------------------------

num_header_cosmorate = 15  # number of variables in cosmorate header
del_cosrate = "\t"  # separator used in CosmoRate catalogs
range_z = 2.0  # range of redshift considered for the analysis
num_cat = 300000  # number of sources wanted in the catalog
del_cat = " "
# ---------------------------------------------      Main code       ---------------------------------------------------

# Make sure directories are created
if not os.path.exists("Astro_Models/"):
    os.mkdir("Astro_Models/")
if not os.path.exists("Astro_Models/Catalogs/"):
    os.mkdir("Astro_Models/Catalogs")
if not os.path.exists("Astro_Models/MergerRateDensity/"):
    os.mkdir("Astro_Models/MergerRateDensity")

# Get all the parameters set in astro_model_param.py
dir_cosmo_rate, astro_param, co_param, mag_gen_param, name_spin_model = return_astro_param(sys.argv)

# CosmoRate processing
process_cosmorate(path_dir_cr=dir_cosmo_rate, num_var_header=num_header_cosmorate, del_cosrate=del_cosrate)

# Initialise spin and astro model
spin_model = SpinModel(name_model=name_spin_model, mag_gen_param=mag_gen_param)
model_astro = AstroModel(astro_model_parameters=astro_param, co_parameters=co_param, spin_model=spin_model,
                         load_cat=False, load_mrd=False)

# Create the merger rate file
model_astro.create_merger_rate_file(dir_cosmorate=dir_cosmo_rate, range_z=range_z)

# Create the catalog
model_astro.create_catalog_file(dir_cosmorate=dir_cosmo_rate, num_cat=num_cat)
