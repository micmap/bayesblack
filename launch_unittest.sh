#!/bin/bash

python -m unittest testing_routines/test_auxiliary_cosmorate.py
python -m unittest testing_routines/test_detector.py
python -m unittest testing_routines/test_gwevent.py
python -m unittest testing_routines/test_observingrun.py
python -m unittest testing_routines/test_spinmodel.py
python -m unittest testing_routines/test_utility_function.py
