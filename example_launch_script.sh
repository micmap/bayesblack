#!/bin/bash

# When calling the scripts with parameters, astro_model_param will uses these parameters
python generate_samples.py param_1_model_1 param_2_model_1 param_3_model_1 &
python generate_samples.py param_1_model_2 param_2_model_2 param_3_model_2 &
python generate_samples.py param_1_model_3 param_2_model_3 param_3_model_3 &

