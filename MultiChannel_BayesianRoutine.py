import numpy as np
import time
from Project_Modules.astro_model import AstroModel
from Project_Modules.bayes_model import BayesModel
from Project_Modules.observing_run import ObservingRun
from Project_Modules.detector import DetectorGW
from Project_Modules.utility_functions import compute_log_likelihood, jump_mix_frac
import os
os.environ['MKL_NUM_THREADS'] = '1'  # this command prevents Python from multithreading
                                     # (useful especially for Demoblack machine!)

# ----------------------------------      User input and initialisation     ------------------------------------

# Definition of astro-model properties
co_type = "BBH"
sigma_met = 0.2
mag_spin = 0.1
alpha = 1.0
co_param = ["Mc", "q", "z", "chieff"]

# List of formation channels
formation_channels = ["Iso", "YSC", "NSC", "GC"]

# Option for the Bayesian analysis
bayes_opt = "NoRate"

# List observatios / detector
observing_runs_name = ["O1", "O2", "O3a"]
det_name = ["Livingston_O1", "Livingston_O2", "Livingston_O3a"]

# MCMC chain
N_mcmc = 100000  # total stops for the MCMC chain
scale_jump = 0.01  # scale used for the gaussian jump
namefile_MCMC = "MCMC_met_" + str(sigma_met) + "_spin_" + str(mag_spin) + "_alpha_" \
           + str(alpha) + "_" + bayes_opt + ".dat"

# Load all the models (data_load set to 0 to speed up time and because the moel data are not needed for the analysis)
models_dict = {}
observing_runs = {}
for o in observing_runs_name:
    observing_runs[o] = ObservingRun(o)

for f in formation_channels:
    for o, d in zip(observing_runs_name, det_name):
        astro_model_param = {"co_type": co_type, "formation_channel": f, "sigma_logmetallicity": sigma_met,
                             "mag_spin": mag_spin, "alpha": alpha}
        model_astro = AstroModel(astro_model_param, co_param, load_cat=False, load_mrd=True)
        detector = DetectorGW(d)
        models_dict[(f, o)] = BayesModel(model_astro, observing_runs[o], detector, read_match=True, read_eff=True)

# Initialisation
mcmc_chain = np.zeros((N_mcmc, len(formation_channels)+1))  # initialise chain, last column contains log-likelihood


# -------------------------------------------      Main code       -------------------------------------------------

# -----------------     Starting MCMC point     -----------------

# Generate a value of hyperparameter hyp randomly within the prior ranges
mix_frac_ini = np.random.dirichlet(np.ones(len(formation_channels)), 1)[0]

# Compute log-likelihood
log_likelihood_ini = 0.0
for o in observing_runs:
    detection_efficiency = 0.0
    n_sources = 0.0
    n_obs = observing_runs[o].n_det[co_type]
    match_sources = []
    for i, f in enumerate(formation_channels):
        detection_efficiency += mix_frac_ini[i] * models_dict[(f, o)].efficiency
        n_sources += mix_frac_ini[i] * models_dict[(f, o)].n_sources
        if i == 0:
            match_sources = mix_frac_ini[i] * np.array(list(models_dict[(f, o)].match_model.values()))
        else:
            match_sources += mix_frac_ini[i] * np.array(list(models_dict[(f, o)].match_model.values()))
    integral_match_model = np.log(match_sources).sum()

    log_likelihood_ini += compute_log_likelihood(bayes_opt, integral_match_model, n_obs, n_sources,
                                                detection_efficiency)


# Record the initial point values and assign them to current position of the chain
mcmc_chain[0, :-1] = mix_frac_ini
mcmc_chain[-1] = log_likelihood_ini
mix_frac_cur = mix_frac_ini
log_likelihood_cur = log_likelihood_ini

# -----------------     Main chain     -----------------

start = time.perf_counter()
accept = 1
for n in range(N_mcmc - 1):
    if n % 25000 == 0:
        print("Completion : " + str(100.0 * float(n) / float(N_mcmc)) + " %")

    # Jump proposal for mixing fraction
    mix_frac_jump = jump_mix_frac(mix_frac_cur, scale_jump)

    # Compute log-likelihood
    log_likelihood_jump = 0.0
    for o in observing_runs:
        detection_efficiency = 0.0
        n_sources = 0.0
        n_obs = observing_runs[o].n_det[co_type]

        match_sources = []
        for i, f in enumerate(formation_channels):
            detection_efficiency += mix_frac_jump[i] * models_dict[(f, o)].efficiency
            n_sources += mix_frac_jump[i] * models_dict[(f, o)].n_sources
            if i == 0:
                match_sources = mix_frac_jump[i] * np.array(list(models_dict[(f, o)].match_model.values()))
            else:
                match_sources += mix_frac_jump[i] * np.array(list(models_dict[(f, o)].match_model.values()))
        integral_match_model = np.log(match_sources).sum()

        log_likelihood_jump += compute_log_likelihood(bayes_opt, integral_match_model,
                                                    n_obs, n_sources, detection_efficiency)

    # Accept jump with probability Metropolis-Hastings ratio
    Hratio = np.exp(log_likelihood_jump - log_likelihood_cur)
    if Hratio > 1:
        mix_frac_cur = mix_frac_jump
        log_likelihood_cur = log_likelihood_jump
        accept += 1
    else:
        beta = np.random.uniform()
        if Hratio > beta:
            mix_frac_cur = mix_frac_jump
            log_likelihood_cur = log_likelihood_jump
            accept += 1

    # Update chain values
    mcmc_chain[n + 1, :-1] = mix_frac_cur
    mcmc_chain[n + 1, -1] = log_likelihood_cur

# Take 1 out of 200 points to ensure a good independance of the samples
mcmc_chain = mcmc_chain[::200]

# Create the file containing the output
print("Acceptance rate is : {} %".format(100.0*float(accept) / float(N_mcmc)))
finish = time.perf_counter()
print(f'Finished in {round(finish - start, 2)} second(s)')

header_file = "\t".join(["f_"+f for f in formation_channels]) + "\t" + "Likelihood" + "\n"

with open(namefile_MCMC, "w") as fileout:
    fileout.write("\t".join(["f_"+f for f in formation_channels]) + "\t" + "Likelihood" + "\n")  # header
    np.savetxt(fileout, mcmc_chain, delimiter='\t', fmt='%.4f')
