import pandas as pd
import sys
import os
from Project_Modules.spin_model import SpinModel
from Project_Modules.astro_model import AstroModel
from astro_model_param import return_astro_param
os.environ['MKL_NUM_THREADS'] = '1' # this command prevents Python from multithreading 
                                    #(useful especiallfy for Demoblack machine!)


if __name__ == '__main__':

    # -------------------------------------------      User input       ------------------------------------------------

    num_samples = 100 #100 #10000  # number of samples wanted
    n_cpu = 1  # number of CPUs
    n_walkers = 16  # number of MCMC walkers
    n_chain = 500 #50 #500  # length of MCMC chain
    bw_method = 0.075  # KDE bandwidth to use

    # -------------------------------------------      Main code       -------------------------------------------------

    # Get all the parameters set in astro_model_param.py
    _, astro_param, co_param, mag_gen_param, name_spin_model = return_astro_param(sys.argv)

    # Initialise spin and astro model
    spin_model = SpinModel(name_model=name_spin_model, mag_gen_param=mag_gen_param)
    astro_model = AstroModel(astro_model_parameters=astro_param, co_parameters=co_param, spin_model=spin_model,
                             load_cat=True, load_mrd=True)

    # Generate num_samples from the astro_model using MCMC
    samples_final = pd.DataFrame()
    n = 0
    while n < num_samples:
        samples = astro_model.sample_catalog(n_walkers=n_walkers, n_chain=n_chain,
                                             bw_method=bw_method, n_cpu=n_cpu)
        samples_final = samples_final.append(samples)
        n = len(samples_final)
        print(n)

    # Make sure directories for samples are created
    if not os.path.exists("Samples/"):
        os.mkdir("Samples/")

    # Export the samples to a file
    sample_file_name = "sampling_" + "_".join([astro_model.map_name_par[x] + "_" +
                                               str(astro_model.astro_model_parameters[x]) for x in
                                               astro_model.astro_model_parameters]) + ".dat"
    samples_final.to_csv("Samples/"+sample_file_name, sep="\t", index=False, float_format="%.4f")
